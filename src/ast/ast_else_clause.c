#include "ast.h"

struct ast_else_clause *ast_init_else_clause(void)
{
    struct ast_else_clause *kasbar = malloc(sizeof(struct ast_else_clause));
    kasbar->is_else = 0;
    kasbar->else_comp_list = NULL;

    kasbar->elif_condition = NULL;
    kasbar->then_condition = NULL;
    kasbar->else_clause = NULL;
    return kasbar;
}

/**
** \fn int exec_else_clause(struct ast_else_clause *node)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_else_clause
** \return le resultat de ast_eval
*/

int exec_else_clause(struct ast_else_clause *node)
{
    if (node == NULL)
        return 0;
    if (node->is_else == 1)
        return ast_eval(node->else_comp_list);

    if (ast_eval(node->elif_condition) == 0)
    {
        if (ast_eval(node->then_condition) != 0)
            return 1;
        return 0;
    }
    if (node->else_clause)
    {
        if (ast_eval(node->else_clause) != 0)
            return 1;
    }
    return 0;
}
void ast_free_else_clause(struct ast_else_clause *ast)
{
    if (ast->else_comp_list != NULL)
        ast_free(ast->else_comp_list);
    if (ast->elif_condition)
        ast_free(ast->elif_condition);
    if (ast->then_condition)
        ast_free(ast->then_condition);
    if (ast->else_clause)
        ast_free(ast->else_clause);

    free(ast);
}
