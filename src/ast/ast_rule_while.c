#include "ast.h"

struct ast_while *ast_init_while(void)
{
    struct ast_while *ne = malloc(sizeof(struct ast_while));
    ne->condition = NULL;
    ne->execution = NULL;
    return ne;
}

void ast_free_while(struct ast_while *ast)
{
    ast_free(ast->condition);
    ast_free(ast->execution);
    free(ast);
}

/**
** \fn int exec_while_node(struct ast_while *node)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_while
** \return 0 ou le résultat de ast_eval
*/

int exec_while_node(struct ast_while *node)
{
    if (!node)
        return 0;
    while (ast_eval(node->condition) == 0)
    {
        if (s->continueflag > 0)
        {
            s->continueflag--;
            if (s->continueflag > 0)
                return 0;
            continue;
        }
        ast_eval(node->execution);
        if (s->breakflag > 0)
        {
            s->breakflag--;
            return 0;
        }
    }
    return 0;
}
