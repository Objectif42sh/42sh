#include "ast.h"

struct ast_case_item *ast_init_case_item(void)
{
    struct ast_case_item *ast = malloc(sizeof(struct ast_case_item));
    ast->pattern = NULL;
    ast->comp_list = NULL;
    ast->variable = NULL;
    return ast;
}

void ast_free_case_item(struct ast_case_item *ast)
{
    if (ast == NULL)
        return;
    if (ast->pattern != NULL)
        free(ast->pattern);
    ast_free(ast->comp_list);
    free(ast);
}

/**
** \fn int exec_case_item(struct ast_case_item *ast)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_case_item
** \return le resultat de ast_eval
*/

int exec_case_item(struct ast_case_item *ast)
{
    return ast_eval(ast->comp_list);
}
