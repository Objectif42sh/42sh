#include "ast.h"

struct ast_and_or *ast_init_and_or(void)
{
    struct ast_and_or *new = malloc(sizeof(struct ast_and_or));
    new->and_or_or = -1;
    new->left = NULL;
    new->right = NULL;
    return new;
}

void ast_free_and_or(struct ast_and_or *ast)
{
    if (ast->left)
        ast_free(ast->left);
    if (ast->left)
        ast_free(ast->right);
    free(ast);
}

/**
** \fn int exec_and_or(struct ast_and_or *ast)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type and_or
** \return 0 si ca à fonctionné, 1 sinon
*/

int exec_and_or(struct ast_and_or *ast)
{
    if (ast == NULL)
        return 0;
    if (s->breakflag > 0 || s->continueflag > 0)
        return 0;
    if (ast->and_or_or == 0)
    {
        int tmp1 = 0;
        tmp1 = ast_eval(ast->left);
        if (tmp1 == 0)
            return tmp1;
        if (s->breakflag > 0 || s->continueflag > 0)
            return 0;
        return ast_eval(ast->right);
    }
    if (ast->and_or_or == 1)
    {
        int tmp2 = 0;
        tmp2 = ast_eval(ast->left);
        if (tmp2 != 0)
            return tmp2;
        if (s->breakflag > 0 || s->continueflag > 0)
            return 0;
        return ast_eval(ast->right);
    }
    return 1;
}
