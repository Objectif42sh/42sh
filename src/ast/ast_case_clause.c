#define _POSIX_C_SOURCE 200809L
#include <string.h>

#include "ast.h"

struct ast_case_clause *ast_init_case_clause(void)
{
    struct ast_case_clause *niew = malloc(sizeof(struct ast_case_clause));
    niew->nbchild = 0;
    niew->variable = NULL;
    return niew;
}

void ast_free_case_clause(struct ast_case_clause *ast)
{
    for (size_t i = 0; i < ast->nbchild; i++)
        ast_free(ast->childlist[i]);
    free(ast);
}

int check_pattern_variable(char **pattern, char *variable)
{
    if (pattern == NULL || variable == NULL)
        return 0;
    for (size_t i = 0; pattern[i] != NULL; i++)
    {
        if (pattern[i] && strcmp(pattern[i], variable) == 0)
            return 1;
    }
    return 0;
}

/**
** \fn int exec_case_clause(struct ast_case_clause *ast)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_case_clause
** \return le resultat de ast_eval
*/

int exec_case_clause(struct ast_case_clause *ast)
{
    for (size_t i = 0; i < ast->nbchild; i++)
    {
        char **pattern = ast->childlist[i]->data.node_case_item->pattern;
        char *variable =
            strdup(ast->childlist[i]->data.node_case_item->variable);
        char *tmp = variable;
        if (contain_var(variable) == 1)
        {
            variable = var_replace(variable);
            free(tmp);
        }
        if (check_pattern_variable(pattern, variable) == 1)
        {
            int res = ast_eval(ast->childlist[i]);
            if (variable)
                free(variable);
            return res;
        }
        if (variable)
            free(variable);
    }
    return 0;
}
