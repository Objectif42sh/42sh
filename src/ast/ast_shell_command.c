#include "ast.h"

struct ast_shell_command *ast_init_shell_command(void)
{
    struct ast_shell_command *ne = malloc(sizeof(struct ast_shell_command));
    ne->compound_or_rule = NULL;
    return ne;
}
void ast_free_shell_command(struct ast_shell_command *ast)
{
    if (ast->compound_or_rule)
        ast_free(ast->compound_or_rule);
    free(ast);
}

/**
** \fn int exec_shell_command(struct ast_shell_command *node)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_shell_command
** \return 0 ou le résultat de ast_eval
*/

int exec_shell_command(struct ast_shell_command *node)
{
    if (node == NULL)
    {
        return 0;
    }
    if (ast_eval(node->compound_or_rule) != 0)
        return 1;
    return 0;
}
