#define _POSIX_C_SOURCE 200809L
#include <string.h>

#include "ast.h"

struct ast_for *ast_init_for(void)
{
    struct ast_for *ne = malloc(sizeof(struct ast_for));
    ne->variable = NULL;
    ne->execution = NULL;
    ne->values = NULL;
    ne->nb_values = 0;
    return ne;
}

void ast_free_for(struct ast_for *ast)
{
    if (ast == NULL)
        return;
    ast_free(ast->execution);
    free(ast->values);
    free(ast);
}

/**
** \fn int exec_for_node(struct ast_for *node)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_for
** \return le resultat de ast_eval
*/

int exec_for_node(struct ast_for *node)
{
    if (node == NULL)
        return 0;
    for (size_t i = 0; i < node->nb_values; i++)
    {
        if (s->continueflag > 0)
        {
            s->continueflag--;
            if (s->continueflag > 0)
                return 0;
        }
        struct var_item *new_var = malloc(sizeof(struct var_item));
        new_var->name = strdup(node->variable);
        new_var->value = strdup(node->values[i]);
        new_var->next = NULL;
        add_var(new_var);
        ast_eval(node->execution);
        if (s->breakflag > 0)
        {
            s->breakflag--;
            return 0;
        }
    }
    return 0;
}
