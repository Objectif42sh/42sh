#include "ast.h"

int check_brea(char *cmd)
{
    return strncmp(cmd, "break", 5) == 0 && (cmd[5] == '\0' || cmd[5] == ' ');
}

int check_con(char *cmd)
{
    return strncmp(cmd, "continue", 8) == 0
        && (cmd[8] == '\0' || cmd[8] == ' ');
}
