#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "ast.h"
struct ast_command *ast_init_command(void)
{
    struct ast_command *ne = malloc(sizeof(struct ast_command));
    ne->simple_or_shell_cmd = NULL;
    ne->redirection = NULL;
    return ne;
}

void ast_free_command(struct ast_command *ast)
{
    if (ast->simple_or_shell_cmd)
        ast_free(ast->simple_or_shell_cmd);
    if (ast->redirection)
        ast_free(ast->redirection);
    free(ast);
}

/**
** \fn int exec_command_node(struct ast_command *node)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_command
** \return le resultat de ast_eval
*/

int exec_command_node(struct ast_command *node)
{
    if (node == NULL)
        return 0;
    if (node->redirection == NULL)
    {
        if (ast_eval(node->simple_or_shell_cmd) != 0)
            return 1;
        return 0;
    }
    return 0;
}
