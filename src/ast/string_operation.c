#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

size_t occ_count(char c, const char *str)
{
    size_t i = 0;
    size_t occ = 0;
    while (*(str + i) != '\0')
    {
        if (*(str + i) == c)
            occ++;
        i++;
    }
    return occ;
}

size_t strlength(const char *str)
{
    size_t length = 0;
    while (*(str + length) != '\0')
        length++;
    return length;
}

void *checkNULL(char *ptr)
{
    if (ptr == NULL)
        return NULL;
    else
        return ptr;
}

/**
** \fn char *quote_replaces(char *rep, char *orig, char *with)
** \brief remplace certaines quotes par d'autre pour que tout marche
**
** \param char *rep, char *orig, char *with
** \return NULL ou le resultat du malloc
*/

char *quote_replaces(char *rep, char *orig, char *with)
{
    char *result;
    char *tmp;
    if (!orig || !rep)
        return NULL;
    int len_rep = strlen(rep);
    if (!with)
        with = "";
    int len_with = strlen(with);
    int len_front;
    int count;

    char *ins = orig;
    for (count = 0; (tmp = strstr(ins, rep)); ++count)
        ins = tmp + len_rep;
    tmp = result = malloc(strlen(orig) + (len_with - len_rep) * count + 1);
    if (!result)
        return NULL;
    while (count--)
    {
        ins = strstr(orig, rep);
        len_front = ins - orig;
        tmp = strncpy(tmp, orig, len_front) + len_front;
        tmp = strcpy(tmp, with) + len_with;
        orig += len_front + len_rep;
    }
    strcpy(tmp, orig);
    return result;
}
