#include "ast.h"

struct ast_compound_list *ast_init_compound_list(void)
{
    struct ast_compound_list *tususs = malloc(sizeof(struct ast_compound_list));
    tususs->nbchild = 0;
    return tususs;
}

/**
** \fn int exec_compound_list(struct ast_compound_list *ast)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_compound_list
** \return le resultat de ast_eval
*/

int exec_compound_list(struct ast_compound_list *ast)
{
    if (s->breakflag > 0 || s->continueflag > 0)
        return 0;
    int errr = 0;
    for (size_t i = 0; i < ast->nbchild; i++)
    {
        errr = ast_eval(ast->childlist[i]);
        if (s->breakflag > 0 || s->continueflag > 0)
            return 0;
    }
    if (errr != 0)
        return 1;
    return 0;
}
void ast_free_compound_list(struct ast_compound_list *ast)
{
    for (size_t i = 0; i < ast->nbchild; i++)
    {
        ast_free(ast->childlist[i]);
    }
    free(ast);
}
