#include "ast.h"

struct ast_list *ast_init_list(void)
{
    struct ast_list *new = malloc(sizeof(struct ast_list));
    new->nbchild = 0;
    return new;
}

void ast_free_list(struct ast_list *ast)
{
    for (size_t i = 0; i < ast->nbchild; i++)
    {
        ast_free(ast->childlist[i]);
    }
    free(ast);
}

/**
** \fn int exec_list(struct ast_list *ast)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_list
** \return le resultat de ast_eval
*/

int exec_list(struct ast_list *ast)
{
    int errr = 0;
    for (size_t i = 0; i < ast->nbchild; i++)
    {
        errr += ast_eval(ast->childlist[i]);
    }
    if (errr != 0)
        return 1;
    return 0;
}
