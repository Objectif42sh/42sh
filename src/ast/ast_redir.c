#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "ast.h"
struct ast_redir *ast_init_redir(void)
{
    struct ast_redir *ne = malloc(sizeof(struct ast_redir));
    ne->is_finishe = 0;
    ne->redirIO_0 = NULL;
    ne->redirIO_1 = NULL;
    ne->redirIO_2 = NULL;
    ne->fd0 = 0;
    ne->fd1 = 0;
    ne->fd2 = 0;
    return ne;
}

void ast_free_redir(struct ast_redir *ast)
{
    free(ast);
}
