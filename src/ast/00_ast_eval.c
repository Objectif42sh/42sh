#include "ast.h"

/**
 * \fn int ast_eval(struct ast *ast)
 * \brief Fonction qui permet de faire le lien entre ast et exec
 *
 * \param ast: noeud de l'ast dont il faut checker le type
 * \return fonction exec qui correspond au type du noeud
 */

static int ast_eval2(struct ast *ast)
{
    if (ast->type == NODE_ELSE_CLAUSE)
        return exec_else_clause(ast->data.node_else_clause);
    else if (ast->type == NODE_LIST)
        return exec_list(ast->data.node_list);
    else if (ast->type == NODE_PIPELINE)
        return exec_pipeline(ast->data.node_pipeline);
    if (ast->type == NODE_FOR)
        return exec_for_node(ast->data.node_for);
    if (ast->type == NODE_IF)
        return exec_if_node(ast->data.node_if);
    if (ast->type == NODE_UNTIL)
        return exec_until_node(ast->data.node_until);
    if (ast->type == NODE_WHILE)
        return exec_while_node(ast->data.node_while);
    if (ast->type == NODE_SHELL_COMMAND)
        return exec_shell_command(ast->data.node_shell_command);
    else if (ast->type == NODE_SIMPLE_COMMAND)
        return exec_simple_command(ast->data.node_simple_cmd);
    return 1;
}

/**
 * \fn int ast_eval(struct ast *ast)
 * \brief Fonction qui permet de faire le lien entre ast et exec
 *
 * \param ast: noeud de l'ast dont il faut checker le type
 * \return fonction exec qui correspond au type du noeud
 */

int ast_eval(struct ast *ast)
{
    if (ast == NULL)
        return 0;
    else if (ast->type == NODE_AND_OR)
        return exec_and_or(ast->data.node_and_or);
    if (ast->type == NODE_CASE)
        return exec_case(ast->data.node_case);
    if (ast->type == NODE_CASE_CLAUSE)
        return exec_case_clause(ast->data.node_case_clause);
    if (ast->type == NODE_CASE_ITEM)
        return exec_case_item(ast->data.node_case_item);
    else if (ast->type == NODE_COMMAND)
        return exec_command_node(ast->data.node_command);
    if (ast->type == NODE_COMPOUND_LIST)
        return exec_compound_list(ast->data.node_compound_list);
    return ast_eval2(ast);
}
