#include "ast.h"

struct ast *ast_init(void)
{
    struct ast *new = malloc(sizeof(struct ast));
    new->type = UNSET;
    return new;
}

/**
** \fn void fri(struct ast *ast)
** \brief Fonction qui permet de faire le lien entre ast et free
**
** \param ast: noeud de l'ast dont il faut checker le type
** \return fonction free qui correspond au type du noeud
*/

static void fri(struct ast *ast)
{
    if (ast->type == NODE_COMPOUND_LIST)
        ast_free_compound_list(ast->data.node_compound_list);
    if (ast->type == NODE_ELSE_CLAUSE)
        ast_free_else_clause(ast->data.node_else_clause);
    if (ast->type == NODE_WHILE)
        ast_free_while(ast->data.node_while);
    if (ast->type == NODE_UNTIL)
        ast_free_until(ast->data.node_until);
    if (ast->type == NODE_FOR)
        ast_free_for(ast->data.node_for);
    if (ast->type == NODE_CASE)
        ast_free_case(ast->data.node_case);
    if (ast->type == NODE_CASE_CLAUSE)
        ast_free_case_clause(ast->data.node_case_clause);
    if (ast->type == NODE_CASE_ITEM)
        ast_free_case_item(ast->data.node_case_item);
    if (ast->type == NODE_REDIR)
        ast_free_redir(ast->data.node_redir);
}

/**
** \fn void choose_free(struct ast *ast)
** \brief Fonction qui permet de faire le lien entre ast et free
**
** \param ast: noeud de l'ast dont il faut checker le type
** \return fonction free qui correspond au type du noeud
*/

static void choose_free(struct ast *ast)
{
    if (ast == NULL || ast->type == UNSET)
    {
        if (ast != NULL)
            free(ast);
        return;
    }
    if (ast->type == NODE_IF)
        ast_free_if(ast->data.node_if);
    if (ast->type == NODE_AND_OR)
        ast_free_and_or(ast->data.node_and_or);
    if (ast->type == NODE_COMMAND)
        ast_free_command(ast->data.node_command);
    if (ast->type == NODE_PIPELINE)
        ast_free_pipeline(ast->data.node_pipeline);
    if (ast->type == NODE_SIMPLE_COMMAND)
        ast_free_simple_command(ast->data.node_simple_cmd);
    if (ast->type == NODE_LIST)
        ast_free_list(ast->data.node_list);
    if (ast->type == NODE_SHELL_COMMAND)
        ast_free_shell_command(ast->data.node_shell_command);
    fri(ast);
    free(ast);
}

void ast_free(struct ast *ast)
{
    if (ast == NULL)
    {
        return;
    }
    choose_free(ast);
}
