#include "ast.h"

struct ast_until *ast_init_until(void)
{
    struct ast_until *ne = malloc(sizeof(struct ast_until));
    ne->condition = NULL;
    ne->execution = NULL;
    return ne;
}

void ast_free_until(struct ast_until *ast)
{
    ast_free(ast->condition);
    ast_free(ast->execution);
    free(ast);
}

/**
** \fn int exec_until_node(struct ast_until *node)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_unitl
** \return 0 ou le résultat de ast_eval
*/

int exec_until_node(struct ast_until *node)
{
    if (!node)
        return 0;
    while (ast_eval(node->condition) == 1)
    {
        if (s->continueflag > 0)
        {
            s->continueflag--;
            if (s->continueflag > 0)
                return 0;
            continue;
        }
        ast_eval(node->execution);
        if (s->breakflag > 0)
        {
            s->breakflag--;
            return 0;
        }
    }
    return 0;
}
