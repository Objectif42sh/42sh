#include "ast.h"

struct ast_pipeline *ast_init_pipeline(void)
{
    struct ast_pipeline *new = malloc(sizeof(struct ast_pipeline));
    new->nbchild = 0;
    new->neg = 0;
    return new;
}

/**
** \fn static int exec2(struct ast_pipeline *ast)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_pipeline
** \return le resultat de ast_eval
*/

static int exec2(struct ast_pipeline *ast)
{
    int errr = 0;
    if (s->breakflag > 0 || s->continueflag > 0)
        return 0;
    for (size_t i = 0; i < ast->nbchild; i++)
    {
        errr = ast_eval(ast->childlist[i]);
        if (s->breakflag > 0 || s->continueflag > 0)
            return 0;
    }
    if (ast->neg == 1)
    {
        if (errr != 0)
            return 0;
        return 1;
    }
    if (errr != 0)
        return 1;
    return 0;
}

static int *tabpipe(int numero)
{
    int *tab_fds = malloc(sizeof(int) * 2 * (numero));
    for (int i = 0; i < (numero); i++)
    {
        pipe(tab_fds + i * 2);
    }
    return tab_fds;
}

/**
** \fn int aux_child(size_t cnt, size_t numero, int *tab_fds, int j)
** \brief fonction qui close la liste tab_fds
**
** \param size_t cnt, size_t numero, int *tab_fds, int j
** \return un message d'erreur si dup2 a échoué, 0 sinon
*/

int aux_child(size_t cnt, size_t numero, int *tab_fds, int j)
{
    if (cnt + 1 < numero)
    {
        if (dup2(tab_fds[j + 1], 1) < 0)
        {
            fprintf(stderr, "exec_pipeline: error during dup2().1\n");
            return -1;
        }
    }
    if (j != 0)
    {
        if (dup2(tab_fds[j - 2], 0) < 0)
        {
            fprintf(stderr, "exec_pipeline: error during dup2().2\n");
            return -1;
        }
    }
    for (size_t i = 0; i < 2 * numero; i++)
    {
        close(tab_fds[i]);
    }
    return 0;
}

/**
** \fn int exec_pipeline(struct ast_pipeline *ast)
** \brief fonction qui appelle ast_eval pour chaque enfant du noeud
**
** \param ast: noeud de l'ast de type ast_pipeline
** \return le resultat de ast_eval si ca fonctionne, 2 si le fork ne fonctionne
**         pas et wstatus sinon
*/

int exec_pipeline(struct ast_pipeline *ast)
{
    if (ast->nbchild < 2)
        return exec2(ast);
    int wstatus;
    size_t cnt = 0;
    int *tab_fds = tabpipe(ast->nbchild);
    int j = 0;
    while (cnt < ast->nbchild)
    {
        int pid = fork();
        if (pid == 0)
        {
            if (aux_child(cnt, ast->nbchild, tab_fds, j) == -1)
                exit(EXIT_FAILURE);
            int a = ast_eval(ast->childlist[cnt]);
            exit(a);
        }
        else if (pid < 0)
        {
            fprintf(stderr, "exec_pipeline: error during fork()\n");
            return 2;
        }
        cnt++;
        j += 2;
    }
    for (size_t i = 0; i < 2 * ast->nbchild; i++)
        close(tab_fds[i]);
    for (size_t i2 = 0; i2 < ast->nbchild + 1; i2++)
        wait(&wstatus);
    free(tab_fds);
    return WEXITSTATUS(wstatus);
}

void ast_free_pipeline(struct ast_pipeline *ast)
{
    for (size_t i = 0; i < ast->nbchild; i++)
    {
        ast_free(ast->childlist[i]);
    }
    free(ast);
}
