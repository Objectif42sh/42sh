#include "ast.h"

struct ast_if *ast_init_if(void)
{
    struct ast_if *niouw = malloc(sizeof(struct ast_if));
    niouw->if_condition = NULL;
    niouw->then_condition = NULL;
    niouw->else_condition = NULL;
    return niouw;
}
void ast_free_if(struct ast_if *ast)
{
    if (ast->if_condition)
        ast_free(ast->if_condition);
    if (ast->then_condition)
        ast_free(ast->then_condition);
    if (ast->else_condition)
        ast_free(ast->else_condition);
    free(ast);
}

/**
** \fn int exec_if_node(struct ast_if *node)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_if
** \return le resultat de ast_eval
*/

int exec_if_node(struct ast_if *node)
{
    if (node == NULL)
    {
        return 0;
    }
    if (ast_eval(node->if_condition) == 0)
    {
        if (ast_eval(node->then_condition) != 0)
            return 1;
        return 0;
    }
    if (node->else_condition)
    {
        if (ast_eval(node->else_condition) != 0)
            return 1;
    }
    return 0;
}
