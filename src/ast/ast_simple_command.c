#define _POSIX_C_SOURCE 200809L
#include <err.h>
#include <string.h>

#include "ast.h"

struct ast_simple_command *ast_init_simple_command(void)
{
    struct ast_simple_command *nv = malloc(sizeof(struct ast_simple_command));
    nv->cmd = NULL;
    nv->prefix_nb = 0;
    return nv;
}

void add_prefix_ast_scmd(struct ast_simple_command *ast, char *prefix)
{
    ast->prefix[ast->prefix_nb] = prefix;
    (ast->prefix_nb)++;
}

void ast_free_simple_command(struct ast_simple_command *ast)
{
    if (ast->cmd)
        free(ast->cmd);
    for (int i = 0; i < ast->prefix_nb; i++)
    {
        free(ast->prefix[i]);
    }
    free(ast);
}

/**
** \fn static int is_valid_break(char *cmd)
** \brief fonction qui verifie si cmd est un "break"
**
** \param char cmd
** \return -1 si cmd=' ', -2 si cmd!=chiffre, sinon le cmd adéquat
*/

static int is_valid_break(char *cmd)
{
    if (cmd[5] == '\0')
        return 1;
    char *nb = calloc(strlen(cmd), 1);
    for (size_t i = 6; cmd[i] != '\0'; i++)
    {
        if (cmd[i] == ' ')
        {
            free(nb);
            return -1;
        }
        if (cmd[i] < '0' || cmd[i] > '9')
        {
            free(nb);
            return -2;
        }
        nb[i - 6] = cmd[i];
    }
    int res = atoi(nb);
    free(nb);
    return res;
}

/**
** \fn static int is_valid_continue(char *cmd)
** \brief fonction qui verifie si cmd est un "continue"
**
** \param char cmd
** \return -1 si cmd=' ', -2 si cmd!=chiffre, sinon le cmd adéquat
*/

static int is_valid_continue(char *cmd)
{
    if (cmd[8] == '\0')
        return 1;
    char *nb = calloc(strlen(cmd), 1);
    for (size_t i = 9; cmd[i] != '\0'; i++)
    {
        if (cmd[i] == ' ')
        {
            free(nb);
            return -1;
        }
        if (cmd[i] < '0' || cmd[i] > '9')
        {
            free(nb);
            return -2;
        }
        nb[i - 9] = cmd[i];
    }
    int res = atoi(nb);
    free(nb);
    return res;
}

/**
** \fn static int break_aux(struct ast_simple_command *ast)
** \brief fonction qui met a jour l'element breakflag de s, ou affiche une err
**
** \param struct ast_simple_command *ast
** \return -1 si trop d'arg, 128 si l'arg pas valide, 0
*/

static int break_aux(struct ast_simple_command *ast)
{
    int breakarg = is_valid_break(ast->cmd);
    if (breakarg == -1)
    {
        s->breakflag = 1024;
        warnx("break: too many arguments");
        return 1;
    }
    if (breakarg == -2)
    {
        s->breakflag = 1024;
        warnx("break: argument not valid (authorized : integer only)");
        return 128;
    }
    s->breakflag = breakarg;
    return 0;
}

/**
** \fn static int oui(struct ast_simple_command *ast)
** \brief met a jour l'élement continueflag de s, ou affiche une erreur
**
** \param struct ast_simple_command *ast
** \return -1 si trop d'arg, 128 si l'arg pas valide, 0
*/

static int oui(struct ast_simple_command *ast)
{
    int continuearg = is_valid_continue(ast->cmd);
    if (continuearg == -1)
    {
        s->continueflag = 1024;
        warnx("break: too many arguments");
        return 1;
    }
    if (continuearg == -2)
    {
        s->continueflag = 1024;
        warnx("break: argument not valid (authorized : integer only)");
        return 128;
    }
    s->continueflag = continuearg;
    return 0;
}

/**
** \fn int exec_simple_command(struct ast_simple_command *ast)
** \brief fonction qui execute une simple command, en vérifiant sa grammaire
**
** \param struct ast_simple_command *ast
** \return le resultat de la fonction "execution" dans parser.c
*/

int exec_simple_command(struct ast_simple_command *ast)
{
    if (ast == NULL)
        return 0;
    if (ast->prefix_nb != 0)
        for (int i = 0; i < ast->prefix_nb; i++)
        {
            char *var_dup = strdup(ast->prefix[i]);
            add_var(assign_word_to_var(var_dup));
        }
    if (ast->cmd != NULL)
    {
        if (check_brea(ast->cmd))
            return break_aux(ast);
        if (check_con(ast->cmd))
            return oui(ast);
        char *cmd_dup = strdup(ast->cmd);
        if (contain_var(ast->cmd))
        {
            char *tmp = cmd_dup;
            cmd_dup = var_replace(cmd_dup);
            free(tmp);
        }
        char *tmp3 = cmd_dup;
        cmd_dup = quote_replaces("\\\\", cmd_dup, "\\");
        free(tmp3);
        int res = execution(cmd_dup);
        if (res == 1)
            s->return_value = 0;
        free(cmd_dup);
        return res;
    }
    return 0;
}
