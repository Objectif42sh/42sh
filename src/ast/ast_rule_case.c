#include "ast.h"

struct ast_case *ast_init_case(void)
{
    struct ast_case *ast = malloc(sizeof(struct ast_case));
    ast->case_clause = NULL;
    ast->variable = NULL;
    return ast;
}

void ast_free_case(struct ast_case *ast)
{
    ast_free(ast->case_clause);
    free(ast);
}

/**
** \fn int exec_case(struct ast_case *node)
** \brief fonction qui appelle ast_eval afin d'executer le noeud
**
** \param ast: noeud de l'ast de type ast_case
** \return le resultat de ast_eval
*/

int exec_case(struct ast_case *node)
{
    return ast_eval(node->case_clause);
}
