#ifndef AST_H
#define AST_H

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>

#include "../parser/parser.h"

enum ast_type
{
    UNSET = 0,
    NODE_AND_OR,
    NODE_CASE_CLAUSE,
    NODE_CASE_ITEM,
    NODE_COMMAND,
    NODE_COMPOUND_LIST,
    NODE_ELSE_CLAUSE,
    NODE_LIST,
    NODE_PIPELINE,
    NODE_REDIR,
    NODE_CASE,
    NODE_FOR,
    NODE_IF,
    NODE_UNTIL,
    NODE_WHILE,
    NODE_SHELL_COMMAND,
    NODE_SIMPLE_COMMAND,
    NODE_NULL
};

enum redir_type
{
    OUI = 0, // ">"
    NON
};

/*
**structure commune à tous les noeuds:
**un type qui permet de choisir l'élément dans l'union
**
**un noeud 'struct ast' contient un 'type', et une 'data' qui est une struct;
**c'est dans cette struct que l'on stocke le "vrai noeud":
**avec ses propres attributs/ses différents fils
*/

struct ast
{
    enum ast_type type;
    union
    {
        struct ast_and_or *node_and_or;
        struct ast_case *node_case;
        struct ast_case_clause *node_case_clause;
        struct ast_case_item *node_case_item;
        struct ast_command *node_command;
        struct ast_compound_list *node_compound_list;
        struct ast_else_clause *node_else_clause;
        struct ast_list *node_list;
        struct ast_pipeline *node_pipeline;
        struct ast_redir *node_redir;
        struct ast_for *node_for;
        struct ast_if *node_if;
        struct ast_until *node_until;
        struct ast_while *node_while;
        struct ast_shell_command *node_shell_command;
        struct ast_simple_command *node_simple_cmd;
    } data;
};

struct ast_list
{
    struct ast *childlist[300]; //**
    size_t nbchild; // nombre de childs dans la childlist
};

struct ast_and_or
{
    int and_or_or; //    0=|| --- 1=&&
    struct ast *left;
    struct ast *right;
};

struct ast_pipeline
{
    struct ast *childlist[124];
    size_t nbchild; // nombre de childs dans la childlist
    int neg;
};

struct ast_command
{
    struct ast *simple_or_shell_cmd; // soit simple soit shell command
    //=> si c une shell command yarua une redirection a gérer
    struct ast *redirection; // si shellcommand
    // funcdec et redirection iront pas dans ast (c comme variables)
};

struct ast_simple_command
{
    char *prefix[300];
    int prefix_nb;
    struct ast *redirection;
    char *cmd;
};

struct ast_shell_command
{
    struct ast *compound_or_rule;
};

/* dès qu'o a une redirection, enregistre ionuber et destination, a  chaque
** redir, si ionumber est le meme, remplacer la destination de l'ancienne
** par la nouvelle
*/

struct ast_redir
{
    int is_finishe;
    char *redirIO_0; // input
    char *redirIO_1; // default (output)
    char *redirIO_2; // err
    int fd0;
    int fd1;
    int fd2;
};

struct ast_compound_list
{
    struct ast *childlist[300];
    size_t nbchild; // nombre de childs dans la childlist
};

struct ast_for
{
    char *variable;
    char **values;
    size_t nb_values;
    struct ast *execution;
};

struct ast_while
{
    struct ast *condition;
    struct ast *execution;
};

struct ast_until
{
    struct ast *condition;
    struct ast *execution;
};
struct ast_case
{
    char *variable;
    struct ast *case_clause; // option: on l'init et on le free meme si pas use
};
struct ast_case_clause
{
    char *variable;
    struct ast *childlist[300];
    size_t nbchild; // nombre de childs dans la childlist
};
struct ast_case_item
{
    char *variable;
    char **pattern;
    struct ast *comp_list;
};
struct ast_if
{
    // if A then B (else C)? fi
    struct ast *if_condition; // A
    struct ast *then_condition; // B
    struct ast *else_condition; // C - optionnel
};

struct ast_else_clause
{
    int is_else; // 1=else -- 2=elif
    struct ast *else_comp_list;

    // elif A then B (else C)?
    struct ast *elif_condition; // A
    struct ast *then_condition; // B
    struct ast *else_clause; // C - optionnel
};

/*
**functions et autres
**faut 1fonction free par structure
*/
struct ast *ast_init(void);
int ast_eval(struct ast *ast);
void ast_free(struct ast *ast);
/*
** (appelle les bons free pour chaque noeud)
*/

struct ast_and_or *ast_init_and_or(void);
int exec_and_or(struct ast_and_or *ast);
void ast_free_and_or(struct ast_and_or *ast);

struct ast_case *ast_init_case(void);
void ast_free_case(struct ast_case *ast);
int exec_case(struct ast_case *ast);

struct ast_case_clause *ast_init_case_clause(void);
int exec_case_clause(struct ast_case_clause *ast);
void ast_free_case_clause(struct ast_case_clause *ast);

struct ast_case_item *ast_init_case_item(void);
int exec_case_item(struct ast_case_item *ast);
void ast_free_case_item(struct ast_case_item *ast);

struct ast_command *ast_init_command(void);
int exec_command_node(struct ast_command *node);
void ast_free_command(struct ast_command *ast);

struct ast_compound_list *ast_init_compound_list(void);
int exec_compound_list(struct ast_compound_list *node);
void ast_free_compound_list(struct ast_compound_list *ast);

struct ast_else_clause *ast_init_else_clause(void);
int exec_else_clause(struct ast_else_clause *node);
void ast_free_else_clause(struct ast_else_clause *ast);

struct ast_list *ast_init_list(void);
int exec_list(struct ast_list *ast);
void ast_free_list(struct ast_list *ast);

struct ast_pipeline *ast_init_pipeline(void);
int exec_pipeline(struct ast_pipeline *node);
void ast_free_pipeline(struct ast_pipeline *ast);

struct ast_redir *ast_init_redir(void);
void ast_free_redir(struct ast_redir *ast);

struct ast_for *ast_init_for(void);
void ast_free_for(struct ast_for *ast);
int exec_for_node(struct ast_for *node);

struct ast_if *ast_init_if(void);
int exec_if_node(struct ast_if *node);
void ast_free_if(struct ast_if *ast);

struct ast_while *ast_init_while(void);
int exec_while_node(struct ast_while *node);
void ast_free_while(struct ast_while *ast);

struct ast_until *ast_init_until(void);
int exec_until_node(struct ast_until *node);
void ast_free_until(struct ast_until *ast);

struct ast_shell_command *ast_init_shell_command(void);
void ast_free_shell_command(struct ast_shell_command *ast);
int exec_shell_command(struct ast_shell_command *ast);

struct ast_simple_command *ast_init_simple_command(void);
void add_prefix_ast_scmd(struct ast_simple_command *ast, char *prefix);
void ast_free_simple_command(struct ast_simple_command *ast);
int exec_simple_command(struct ast_simple_command *ast);
struct ast_compound_list *ast_init_compound_list(void);

// PRINT
// etc
char *quote_replaces(char *rep, char *orig, char *with);
int check_con(char *cmd);
int check_brea(char *cmd);

#endif
