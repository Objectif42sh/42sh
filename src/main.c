#define _GNU_SOURCE
#define _POSIX_C_SOURCE 200809L
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "lexer/lexer.h"
#include "parser/parser.h"
#include "shell.h"

struct shell *s = NULL;

/**
** \fn int depuis_fichier(char *filename)
** \brief fonction appellee par main si 42sh est utilise avec un script
**
** \param filename le nom du script a traiter
** \return 0 si pas erreur sinon n importe quel entier
*/
int depuis_fichier(char *filename)
{
    FILE *fd;
    if (filename != NULL)
    {
        fd = fopen(filename, "r");
        if (fd == NULL)
            return 1;
    }
    else
        fd = stdin;
    s = init_shell();
    s->fdflag = fd;
    char *file = calloc(2, sizeof(char));
    char *line = NULL;
    size_t len;
    while (getline(&line, &len, fd) != -1)
    {
        file = realloc(file, (strlen(file) + len) * sizeof(char));
        file = strcat(file, line);
    }
    lexer_add_str(s->lexer, file);
    create_token_list(s->lexer);
    s->line = line;
    s->file = file;
    int oui = parse();
    int res = s->return_value;
    free_shell(s);
    if (oui != 0)
        errx(2, "Error syntax.");
    return res;
}

/**
** \fn int depuis_option_c(char *file)
** \brief fonction que le main appelle si 42sh est appele avec l option c
**
** \param file la commande envoyee a 42sh juste apres l option c
** \return 0 si pas erreur sinon n importe quel entier
*/
int depuis_option_c(char *file)
{
    s = init_shell();

    lexer_add_str(s->lexer, file);
    create_token_list(s->lexer);

    if (parse() != 0)
    {
        free_shell(s);
        errx(2, "Error syntax.");
    }
    int res = s->return_value;
    free_shell(s);
    fflush(NULL);
    return res;
}

/**
** \fn int main(int argc, char **argv)
** \brief fonction main qui est le coeur du programme
**
** \param argc nb d arguments du prgrm, argv contient les arguments du prgrm
** \return 0 si pas erreur sinon n importe quel entier
*/
int main(int argc, char **argv)
{
    int is0p = 0;
    int is0m = 0;
    char *filename = NULL;
    int option_c = 0;
    int i = 1;
    for (; i < argc; i++)
    {
        if (strlen(argv[i]) > 2 && argv[i][0] == '-' && argv[i][1] == '-')
        {
            if (i > 1)
                errx(2, "Usage: 42sh [GNU long option] [option] [file]");
        }
        else if (strcmp(argv[i], "-c") == 0)
        {
            if (i + 1 >= argc)
                errx(2, "Usage: 42sh [GNU long option] [option] [file]");
            option_c = 1;
            break;
        }
        else if (strcmp(argv[i], "-0") == 0 || strcmp(argv[i], "+0") == 0)
        {
            if (is0p != 0 || is0m != 0)
                errx(2, "Usage: 42sh [GNU long option] [option] [file]");
            is0m = (strcmp(argv[i], "-0") == 0) ? i : is0m;
            is0p = (strcmp(argv[i], "+0") == 0) ? i : is0p;
        }
        else
            filename = argv[i];
    }
    if (option_c == 0)
    {
        return depuis_fichier(filename);
    }
    else
    {
        return depuis_option_c(argv[i + 1]);
    }
}
