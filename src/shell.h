#ifndef SHELL_H
#define SHELL_H
#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "lexer/lexer.h"
#define _POSIX_C_SOURCE 200809L
#include "ast/ast.h"
#include "parser/parser.h"

/**
** \struct shell
** \brief objet qui contient tout ce qui est utile au long du programme
**
** La struct shell est utile tout le long du programme
*/
struct shell
{
    struct var_list *vars;

    struct ast *astheads[500];
    size_t nbchild;
    struct lexer *lexer;
    int return_value;
    int parse_error;
    struct ast_redir *redirecsion;
    struct ast_redir *redirecsion_cmd;

    int breakflag;
    int continueflag;
    FILE *fdflag;
    char *file;
    char *line;
};

/**
**variable generale qui contient tout, et quon free a la fin ou si on exit
*/
extern struct shell *s;

struct shell *init_shell(void);
void free_shell();

#endif
