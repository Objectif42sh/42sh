#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lexer.h"

int is_not_separator(int c)
{
    if ((c != ' ') && c != '\t' && c != '\0' && c != '\n' && c != ';'
        && c != '&' && c != '|' && c != '(' && c != ')' && c != '\"'
        && c != '\'')
        return 1;

    return 0;
}

int is_space(int c)
{
    if ((c == ' ') || c == '\t')
        return 1;

    return 0;
}

int is_comment(int c)
{
    if (c == '\n')
        return 0;
    return 1;
}
int is_double_quote(int c)
{
    if (c == '\"' || c == '\0')
        return 0;
    return 1;
}
int is_quote(int c)
{
    if (c == '\'' || c == '\0')
        return 0;
    return 1;
}
