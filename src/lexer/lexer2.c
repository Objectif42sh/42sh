#include "lexer.h"

enum token_type word_assign_word(char *word)
{
    if (*word == '\'')
        return STRONG;
    if (strcmp(word, "!") == 0)
        return EXCLAMATION;
    if (strchr(word, '=') != NULL && strlen(word) > 1)
        return ASSIGN_WORD;
    if (is_keyword(word))
        return RESERVED_WORD;
    if (strcmp(word, "&") == 0)
        return SEMI_COLON;
    if (strcmp(word, "(") == 0)
        return OPEN_PARENTHESIS;
    if (strcmp(word, ")") == 0)
        return CLOSE_PARENTHESIS;
    if (strcmp(word, "|") == 0)
        return PIPE;
    if (strcmp(word, "&&") == 0 || strcmp(word, "||") == 0)
        return TOK_ANDOR;
    if (strchr(word, '>') != NULL)
        return REDIR;
    return WORD;
}

enum token_type get_word_type(char *word)
{
    if (*word == '\n')
        return SPACE_LINE;
    if (*word == ';')
        return SEMI_COLON;
    if (*word == '\0')
        return TOKEN_EOF;
    else
        return word_assign_word(word);
}

void create_token_list(struct lexer *lexer)
{
    if (lexer->str == NULL)
    {
        warnx("str null pendant la lexerisation");
        return;
    }
    char *curr_word = NULL;
    while ((curr_word = get_next_word(lexer)) != NULL)
        lexer_add_token(lexer, curr_word, get_word_type(curr_word));
}

void print_lexer(struct lexer *lexer)
{
    struct token *curr = lexer->token_list;

    while (curr != NULL)
    {
        if (curr->type == TOKEN_EOF)
            printf("enum: %d; word: \\0 \n", curr->type);

        else if (curr->type == SPACE_LINE)
            printf("enum: %d; word: \\n \n", curr->type);

        else
            printf("enum: %d; word: %s\n", curr->type, curr->word);

        curr = curr->next;
    }
}

void free_lexer(struct lexer *lexer)
{
    if (lexer == NULL)
        return;

    struct token *curr = lexer->token_list;
    if (curr == NULL)
        return;
    if (curr->next == NULL)
    {
        free(curr->word);
        free(curr);
        free(lexer);
        return;
    }
    struct token *prev = curr;
    curr = curr->next;
    while (curr->next != NULL)
    {
        if (prev->type != TOKEN_EOF)
            free(prev->word);
        free(prev);
        prev = curr;
        curr = curr->next;
    }
    free(prev->word);
    free(prev);
    free(curr->word);
    free(curr);
    free(lexer);
}
