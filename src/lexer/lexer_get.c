#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lexer.h"

/**
** \fn struct token *get_next_token(struct lexer *lexer)
** \brief permet d aller au prochain token, et de maj le current token
**
** \param lexer le lexer de la struct shell
** \return le prochain token, on utilise pas tjrs la valeur de retour
*/
struct token *get_next_token(struct lexer *lexer)
{
    if (lexer == NULL)
        return NULL;

    if (lexer->current == NULL)
    {
        lexer->current = lexer->token_list;
        return lexer->current;
    }

    lexer->current = lexer->current->next;
    return lexer->current;
}

/**
** \fn enum token_type get_next_type(struct lexer *lexer)
** \brief permet de connaitre le type du next token
**
** \param lexer le lexer de la struct shell
** \return return le type du next token en enum
*/
enum token_type get_next_type(struct lexer *lexer)
{
    if (lexer->current == NULL)
        return lexer->token_list->type;
    if (lexer->current->next == NULL)
        return TOKEN_EOF;
    return lexer->current->next->type;
}
