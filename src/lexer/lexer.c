#include "lexer.h"

#include <err.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
** \fn struct lexer *init_lexer(void)
** \brief permet d initialiser un nouveau lexer
**
** \param void aucun parametre
** \return return un nouveau lexer
*/
struct lexer *init_lexer(void)
{
    struct lexer *lexer = malloc(sizeof(struct lexer));

    if (lexer == NULL)
        return NULL;

    lexer->str = NULL;
    lexer->token_list = NULL;
    lexer->current = NULL;

    return lexer;
}

void lexer_add_str(struct lexer *lexer, char *str)
{
    if (lexer == NULL)
    {
        warnx("lexer NULL alors qu'on essaye d'add");
        return;
    }

    lexer->str = str;
}

struct token *get_last_token(struct lexer *lexer)
{
    if (lexer == NULL)
        return NULL;

    struct token *curr = lexer->token_list;

    if (curr == NULL)
        return NULL;

    while (curr->next != NULL)
    {
        curr = curr->next;
    }

    return curr;
}

void lexer_add_token(struct lexer *lexer, char *word, enum token_type type)
{
    if (lexer == NULL)
    {
        warnx("lexer NULL alors qu'on essaye d'add woula");
        return;
    }
    if (*word == '#')
    {
        free(word);
        return;
    }
    struct token *token = malloc(sizeof(struct token));

    token->word = word;
    token->type = type;
    token->next = NULL;

    struct token *last = get_last_token(lexer);

    if (last == NULL)
        lexer->token_list = token;

    else
        last->next = token;
}

int is_valid_name(char *word)
{
    if (*word >= '0' && *word <= '9')
        return 1;
    for (int i = 1; (word[i] != '\0') && (word[i] != '='); i++)
    {
        if (!((word[i] == '_') || (word[i] >= 'A' && word[i] <= 'z')
              || (word[i] >= '0' && word[i] <= '9')))
            return 1;
    }
    return 0;
}
