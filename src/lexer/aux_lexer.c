#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lexer.h"

void skip_class(int (*classifier)(int c), char **cursor)
{
    while (classifier(**cursor))
        (*cursor)++;
}

char *get_next_word(struct lexer *lexer)
{
    if (lexer == NULL || lexer->str == NULL)
        return NULL;

    if (*(lexer->str) == '\0')
    {
        char *res = calloc(sizeof(char), 2);
        *res = '\0';
        lexer->str = NULL;

        return res;
    }

    char *copy = lexer->str;
    skip_class(is_space, &copy);

    if (*copy == '\n' || *copy == ';' || *copy == '&' || *copy == '|'
        || *copy == ')' || *copy == '(')
    {
        if (copy[1] == '&' || copy[1] == '|')
        {
            lexer->str += strlen(lexer->str) - strlen(copy) + 2;
            char *res = calloc(sizeof(char), 3);
            *res = *copy;
            res[1] = copy[1];
            return res;
        }

        lexer->str += strlen(lexer->str) - strlen(copy) + 1;
        char *res = calloc(sizeof(char), 2);
        *res = *copy;

        return res;
    }

    char *word_begin = copy;
    char *word_last = NULL;
    size_t word_len = 0;

    if (*word_begin == '\"')
    {
        //printf("copy avant double qote: %s \n", copy);

        word_begin++;
        copy++;
        skip_class(is_double_quote, &copy);
        
        //printf("copy arpès skip  double qote: %s \n", copy);

        while (copy != lexer->str && *(copy - 1) == '\\')
        {
            copy++;
            skip_class(is_double_quote, &copy);
            
            //printf("copy arpès skipi while  double qote: %s \n", copy);

        }

        word_last = copy;
        copy++;

          //erreur quoting
        if (*copy == '\0')
        {
            //printf("copy double qote: %s \n", copy);
            s->parse_error = 1;
            return NULL;
        }
    }

    else if (*word_begin == '\'')
    {
        copy++;
        skip_class(is_quote, &copy);
        
        //printf(" copy: %s \n", copy);
        
        while (copy != lexer->str && *(copy - 1) == '\\')
        {
            copy++;
            skip_class(is_quote, &copy);
            //printf(" copy: %s \n", copy);

        }
        copy++;
        word_last = copy;

        //erreur quoting
        if (*copy == '\0')
        {
            s->parse_error = 1;
            return NULL;
        }

    }

    else if (*word_begin == '#')
    {
        skip_class(is_comment, &copy);
        word_last = copy;
    }

    else
    {
        skip_class(is_not_separator, &copy);
        while (*(copy - 1) == '\\')
        {
            copy++;
            skip_class(is_not_separator, &copy);
        }
        word_last = copy;
    }

    if (word_len == 0)
        word_len = word_last - word_begin;

    if (word_len == 0)
        return NULL;

    char *word = calloc(word_len + 1, sizeof(char));
    word = strncpy(word, word_begin, word_len);
    lexer->str += strlen(lexer->str) - strlen(copy);

    //printf("on return %s \n", word);
    return word;
}
