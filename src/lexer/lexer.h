#ifndef LEXER_H
#define LEXER_H
//#include "../shell.h"
#include "../parser/parser.h"
enum token_type
{
    UNVALID = 0,
    WORD,
    ASSIGN_WORD,
    RESERVED_WORD,
    SEMI_COLON,
    EXCLAMATION,
    ESPERLUETTE,
    SPACE_LINE,
    TOKEN_EOF,
    PIPE,
    STRONG,
    OPEN_PARENTHESIS,
    CLOSE_PARENTHESIS,
    TOK_ANDOR,
    REDIR // A TRAITER, CHAQUE CAS à METTRE ICI ?
};

struct token
{
    char *word;
    enum token_type type;

    struct token *next;
};

struct lexer
{
    char *str;

    struct token *token_list;

    struct token *current;
};

// initialise la struct lexer
struct lexer *init_lexer(void);

// ajoute le str qui contient tout le file à la struct lexer
void lexer_add_str(struct lexer *lexer, char *str);

// cree la token list a partir du str
void create_token_list(struct lexer *lexer);

// print toute la token list du lexer
void print_lexer(struct lexer *lexer);

// free toute la struct et tout et tout
void free_lexer(struct lexer *lexer);

void lexer_add_token(struct lexer *lexer, char *word, enum token_type);

char *get_next_word(struct lexer *lexer);
void skip_class(int (*classifier)(int c), char **cursor);
int is_not_separator(int c);
int is_space(int c);
int is_comment(int c);
int is_double_quote(int c);
int is_quote(int c);

// renoie le prochain token et deplace le current dessus
struct token *get_next_token(struct lexer *lexer);

// return le type du prochain token mais sans deplacer le current token
enum token_type get_next_type(struct lexer *lexer);

#endif
