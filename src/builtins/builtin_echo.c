#include <stdlib.h>
#include <unistd.h>

#include "builtins.h"

static int index_max(char *argv[])
{
    int i = 0;
    for (; argv[i] != NULL; i++)
        ;
    return i;
}
static int escape_checker(char c)
{
    if (c == 'r' || c == 'c' || c == 'e' || c == '\\' || c == 'n' || c == 'v'
        || c == 'b' || c == 't' || c == 'a' || c == 'f')
        return 1;
    else
        return 0;
}

static void aux(char c)
{
    FILE *fout = stdout;
    if (c == 'n')
        fprintf(fout, "\n");

    if (c == 'r')
        fprintf(fout, "\r");

    if (c == 't')
        fprintf(fout, "\t");

    if (c == 'v')
        fprintf(fout, "\v");

    if (c == '\\')
        fprintf(fout, "\\");

    if (c == 'a')
        fprintf(fout, "\a");

    if (c == 'b')
        fprintf(fout, "\b");
}
int put_escaped(char c)
{
    FILE *fout = stdout;

    aux(c);

    if (c == 'e')
        fprintf(fout, "\033");

    if (c == 'f')
        fprintf(fout, "\f");

    if (c == 'c')
        return -1;

    return 0;
}

int print_escaped(char *argv)
{
    int i;

    for (i = 0; argv[i] != '\0'; i++)
    {
        if (argv[i] == '\\')
        {
            if (argv[i] != '\0')
            {
                if (escape_checker(argv[i + 1]))
                {
                    if (put_escaped(argv[i + 1]) == -1)
                    {
                        fflush(stdout);
                        return -1;
                    }
                }
                else
                    fprintf(stdout, "%c", argv[i + 1]);
                i++;
            }
        }
        else
            fprintf(stdout, "%c", argv[i]);
    }
    fflush(stdout);
    return 0;
}

static int echo_get_opt(char *argv[], struct opt_flag *opt_flag)
{
    int i = 0;
    int j = 0;

    for (i = 1; i < index_max(argv); i++)
    {
        if (argv[i][0] == '-')
        {
            for (j = 1;
                 argv[i][j] == 'e' || argv[i][j] == 'n' || argv[i][j] == 'E';
                 j++)
            {
                if (argv[i][j] == 'e')
                    opt_flag->eflag = 1;
                if (argv[i][j] == 'n')
                    opt_flag->nflag = 1;
                if (argv[i][j] == 'E')
                    opt_flag->eeflag = 1;
            }
            if ((argv[i][j] != '\0') || (j == 1))
                break;
        }
        else
            break;
    }
    opt_flag->nb = i;

    return index_max(argv);
}

/**
** \fn int main_echo(char *argv[], struct opt_flag *opt_flag, int optind)
** \brief Fonction auxiliaire du builtin echo, qui appelle les autres fun aux
**
** \param argv est cmd et args, opt_flag les options, optind index des opt
** \return -1 si erreur, sinon 0
*/
int main_echo(char *argv[], struct opt_flag *opt_flag, int optind)
{
    int i;
    int argc;
    argc = index_max(argv);
    if (opt_flag->eflag == 0)
    {
        for (i = optind; i < argc - 1; i++)
            fprintf(stdout, "%s ", argv[i]);
        if (argv[i])
            fprintf(stdout, "%s", argv[i]);
    }
    else
    {
        for (i = optind; i < argc - 1; i++)
        {
            if (print_escaped(argv[i]) == -1)
                return -1;
            fprintf(stdout, " ");
        }
        if (argv[i])
            if (print_escaped(argv[i]) == -1)
                return -1;
    }
    return 0;
}

static struct opt_flag init_structure(void)
{
    struct opt_flag opt_flag;

    opt_flag.eeflag = 0;
    opt_flag.eflag = 0;
    opt_flag.nflag = 0;
    opt_flag.nb = 0;
    return opt_flag;
}

/**
** \fn int builtin_echo(char *argv[])
** \brief Fonction builtin echo
**
** \param argv la commande echo et ses arguments
** \return 0 anyways
*/
int builtin_echo(char *argv[])
{
    struct opt_flag opt_flag = init_structure();
    echo_get_opt(argv, &opt_flag);
    if (main_echo(argv, &opt_flag, opt_flag.nb) == -1)
        return 0;
    if (opt_flag.nflag == 0)
        fprintf(stdout, "\n");
    fflush(stdout);
    return 0;
}
