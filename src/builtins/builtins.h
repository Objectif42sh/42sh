#define _POSIX_C_SOURCE 200809L
#include "../ast/ast.h"
#include "../lexer/lexer.h"
#include "../parser/parser.h"
#include "../parser/var.h"
#include "../shell.h"

/**
** \struct opt_flag
** \brief objet flag des differentes options
**
** La struct opt_flag permettant de gerer les options existantes
*/
struct opt_flag
{
    int eflag;
    int nflag;
    int eeflag;
    int nb;
};

int is_a_builtin(char **name);
int choose_builtin(char **cmd);
int my_cd(char **cmd);
int builtin_echo(char **argv);
int builtin_exit(char **cmd);
