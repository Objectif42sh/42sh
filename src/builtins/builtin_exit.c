#include "builtins.h"

/**
** \fn int builtin_exit(char **cmd)
** \brief Fonction builtin exit
**
** \param cmd la commande et ses arguments
** \return 0 si erreur, sinon exit le programme avec la valeur attendue
*/
int builtin_exit(char **cmd)
{
    int res = s->return_value;
    char *tmp;
    if (cmd[1] != NULL)
    {
        int base = 10;
        res = strtol(cmd[1], &tmp, base);
        if (tmp == cmd[1])
        {
            fprintf(stderr, "%s: numeric argument required\n", cmd[1]);
            free_shell(s);
            exit(2);
        }
    }
    if (cmd[2])
    {
        fprintf(stderr, "exit: too many arguments\n");
        return 0;
    }
    free_shell(s);
    exit(res);
}
