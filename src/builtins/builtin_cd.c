#include "builtins.h"

/**
** \fn int my_cd(char **cmd)
** \brief Fonction builtin cd
**
** \param cmd la commande cd et ses arguments
** \return 1 si erreur, 0 sinon
*/
int my_cd(char **cmd)
{
    char str[500];
    char *new = NULL;
    char *home = NULL;

    if ((home = getenv("HOME")) == NULL)
        home = "/";

    if ((new = cmd[1]) == NULL)
        new = home;

    getcwd(str, 500);
    setenv("OLDPWD", str, 1);
    if (chdir(new) == 0)
    {
        getcwd(str, 500);
        setenv("PWD", str, 1);
        return 0;
    }
    else
    {
        fprintf(stderr, "%s: No such file or directory\n", cmd[1]);
        return 1;
    }
}
