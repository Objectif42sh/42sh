#include "builtins.h"

/**
** \fn int is_a_builtin(char **name)
** \brief Fonction qui check si builtin (et si oui execute le bon builtin)
**
** \param name la commande qui est peut etre un builtin, et ses args
** \return -1 si pas un builtin, sinon la valeur de retour du builtin
*/
int is_a_builtin(char **name)
{
    char *builtin_table[3] = {
        "cd", "echo",
        "exit" // etc
    };
    for (int i = 0; i < 3; ++i)
        if (!strcmp(name[0], builtin_table[i]))
            return choose_builtin(name);
    return -1;
}

/**
** \fn int choose_builtin(char **cmd)
** \brief Fonction qui appelle le bon builtin avec cmd
**
** \param cmd la commande qui est un builtin, et ses args
** \return 0 si pas un builtin, sinon la valeur de retour du builtin
*/
int choose_builtin(char **cmd)
{
    if (strcmp("cd", cmd[0]) == 0)
        return my_cd(cmd);
    if (strcmp("echo", cmd[0]) == 0)
        return builtin_echo(cmd);
    if (strcmp("exit", cmd[0]) == 0)
        return builtin_exit(cmd);
    return 0;
}
