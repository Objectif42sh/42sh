#include "parser.h"

/**
** \fn struct ast *parse_while(void)
** \brief parse les rule while
**
** \param void.
** \return ast rempli ou quoi
*/
struct ast *parse_while(void)
{
    struct ast *ast_while = ast_init();
    ast_while->type = NODE_WHILE;
    ast_while->data.node_while = ast_init_while();
    struct token *current = s->lexer->current;

    if (current->type == RESERVED_WORD && strcmp(current->word, "while") == 0)
    {
        get_next_token(s->lexer);
        if ((ast_while->data.node_while->condition = check_compound()) != NULL)
        {
            current = s->lexer->current;
            if (current->type == RESERVED_WORD
                && strcmp(current->word, "do") == 0)
            {
                get_next_token(s->lexer);
                if ((ast_while->data.node_while->execution = check_compound())
                    != NULL)
                {
                    current = s->lexer->current;
                    if (current->type == RESERVED_WORD
                        && strcmp(current->word, "done") == 0)
                    {
                        get_next_token(s->lexer);
                        return ast_while;
                    }
                    else
                        s->parse_error = 1;
                    // pas de done a la fin
                }
                // erreur dans compond list
            }
            else
                s->parse_error = 1;
            // pas de do apres donc erreur
        }
        // condition a rate a cause de compoundlist donc retrun NULL
    }
    // pas de while au debut donc erreur
    ast_free(ast_while);
    return NULL;
}
