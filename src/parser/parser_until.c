#include "parser.h"

/**
** \fn struct ast *parse_until(void)
** \brief parse les rule until
**
** \param void.
** \return ast rempli ou quoi
*/
struct ast *parse_until(void)
{
    struct ast *ast_until = ast_init();
    ast_until->type = NODE_UNTIL;
    ast_until->data.node_until = ast_init_until();
    struct token *current = s->lexer->current;
    if (current->type == RESERVED_WORD && strcmp(current->word, "until") == 0)
    {
        get_next_token(s->lexer);
        if ((ast_until->data.node_until->condition = check_compound()) != NULL)
        {
            current = s->lexer->current;
            if (current->type == RESERVED_WORD
                && strcmp(current->word, "do") == 0)
            {
                get_next_token(s->lexer);
                if ((ast_until->data.node_until->execution = check_compound())
                    != NULL)
                {
                    current = s->lexer->current;
                    if (current->type == RESERVED_WORD
                        && strcmp(current->word, "done") == 0)
                    {
                        get_next_token(s->lexer);
                        return ast_until;
                    }
                    else
                        s->parse_error = 1;
                }
            }
            else
                s->parse_error = 1;
        }
    }
    ast_free(ast_until);
    return NULL;
}
