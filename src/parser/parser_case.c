#include "parser.h"

/**
** \fn static void *surprise(struct ast *ast)
** \brief fonction qui return NULL et free par manque de place ou quoi
**
** \param ast: ast entier
** \return le resultat NULL
*/
static void *surprise(struct ast *ast)
{
    ast_free(ast);
    return NULL;
}

/**
** \fn static void eat_les_n(void)
** \brief fonction qui skip les \n
**
** \param void
** \return rien wola
*/
static void eat_les_n(void)
{
    while (s->lexer->current->type == SPACE_LINE)
        get_next_token(s->lexer);
}
/**
** \fn static char **add_pattern(char **pattern, char *str)
** \brief ajoute un pattern au char** pattern
**
** \param char** pattern: liste des pattern. char* str un pattern
** \return la liste des pattern
*/
static char **add_pattern(char **pattern, char *str)
{
    size_t size = 0;
    if (str == NULL)
        return pattern;
    if (pattern)
    {
        for (size = 0; pattern[size]; ++size)
        {
            ;
        }
    }
    size++;
    pattern = realloc(pattern, (size + 1) * sizeof(char *));
    pattern[size - 1] = str;
    pattern[size] = NULL;
    return pattern;
}

/**
** \fn struct ast *parse_case(void)
** \brief parse une rule case ou quoi
**
** \param void: mais utilise la variable globale
** \return ast rempli
*/
struct ast *parse_case(void)
{
    struct ast *a = ast_init();
    a->type = NODE_CASE;
    a->data.node_case = ast_init_case();
    if (s->lexer->current->type == RESERVED_WORD
        && strcmp(s->lexer->current->word, "case") == 0)
    {
        get_next_token(s->lexer);
        if (s->lexer->current->type == WORD
            || (s->lexer->current->type == RESERVED_WORD
                && strcmp(s->lexer->current->word, "esac") != 0))
        {
            char *variable = s->lexer->current->word;
            get_next_token(s->lexer);
            a->data.node_case->variable = s->lexer->current->word;
            eat_les_n();
            if (s->lexer->current->type == RESERVED_WORD
                && strcmp(s->lexer->current->word, "in") == 0)
            {
                get_next_token(s->lexer);
                eat_les_n();
                a->data.node_case->case_clause = check_case_clause(variable);
                if (s->lexer->current->type == RESERVED_WORD
                    && strcmp(s->lexer->current->word, "esac") == 0)
                {
                    get_next_token(s->lexer);
                    return a;
                }
            }
        }
        s->parse_error = 1;
    }
    ast_free(a);
    return NULL;
}

/**
** \fn struct ast *check_case_clause(char* variable)
** \brief parse une rule case clause
**
** \param char* variable: variable dans le case en haut
** \return ast rempli
*/
struct ast *check_case_clause(char *variable)
{
    struct ast *a_clause = ast_init();
    a_clause->type = NODE_CASE_CLAUSE;
    a_clause->data.node_case_clause = ast_init_case_clause();
    a_clause->data.node_case_clause->variable = variable;
    size_t nbchild = a_clause->data.node_case_clause->nbchild;
    while ((a_clause->data.node_case_clause->childlist[nbchild] =
                check_case_item(variable)))
    {
        nbchild++;
        a_clause->data.node_case_clause->nbchild++;
        if (s->lexer->current->type == SEMI_COLON
            && s->lexer->current->next->type == SEMI_COLON)
        {
            get_next_token(s->lexer);
            get_next_token(s->lexer);
        }
        else
            break;
        eat_les_n();
    }
    if (nbchild == 0)
    {
        ast_free(a_clause);
        return NULL;
    }
    if (s->lexer->current->type == SEMI_COLON
        && s->lexer->current->next->type == SEMI_COLON)
    {
        get_next_token(s->lexer);
        get_next_token(s->lexer);
    }
    eat_les_n();
    return a_clause;
}

/**
** \fn struct ast *check_case_clause(char* variable)
** \brief parse une rule case clause
**
** \param char* variable: variable dans le case en haut
** \return ast rempli
*/
struct ast *check_case_item(char *variable)
{
    struct ast *a_item = ast_init();
    a_item->type = NODE_CASE_ITEM;
    a_item->data.node_case_item = ast_init_case_item();
    a_item->data.node_case_item->variable = variable;
    if (s->lexer->current->type == OPEN_PARENTHESIS)
        get_next_token(s->lexer);
    char **pattern = NULL;
    struct token *oo = s->lexer->current;
    if (oo->type == 1 || (oo->type == 3 && strcmp(oo->word, "esac") != 0))
    {
        pattern = add_pattern(pattern, s->lexer->current->word);
        get_next_token(s->lexer);
        while (s->lexer->current->type == PIPE)
        {
            get_next_token(s->lexer);
            if (s->lexer->current->type != 1 && s->lexer->current->type != 3)
            {
                s->parse_error = 1;
                return surprise(a_item);
            }
            pattern = add_pattern(pattern, s->lexer->current->word);
            get_next_token(s->lexer);
        }
        a_item->data.node_case_item->pattern = pattern;
        if (s->lexer->current->type == CLOSE_PARENTHESIS)
        {
            get_next_token(s->lexer);
            eat_les_n();
            a_item->data.node_case_item->comp_list = check_compound();
            return a_item;
        }
    }
    return surprise(a_item);
}
