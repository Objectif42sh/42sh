#include "parser.h"

/**
** \fn static void *surprise(struct ast *ast)
** \brief fonction qui return NULL et free par manque de place ou quoi
**
** \param ast: ast entier
** \return le resultat NULL
*/
static void *surprise(struct ast *ast)
{
    ast_free(ast);
    return NULL;
}

/**
** \fn struct ast *check_input(void)
** \brief parsing des inputs
**
** \param void.
** \return ast bien rempli oui je pense
*/
struct ast *check_input(void)
{
    struct token *current = get_next_token(s->lexer);
    struct ast *ast_list;
    if (current->type == SPACE_LINE || current->type == TOKEN_EOF)
    {
        struct ast *res = ast_init();
        res->type = NODE_LIST;
        res->data.node_list = ast_init_list();
        return res;
    }
    if ((ast_list = check_list()) != NULL)
    {
        if (s->lexer->current->type == SPACE_LINE
            || s->lexer->current->type == TOKEN_EOF)
        {
            return ast_list;
        }
    }
    ast_free(ast_list);
    return NULL;
}

/**
** \fn struct ast *check_input(void)
** \brief parsing des inputs
**
** \param void.
** \return ast bien rempli oui je pense
*/
struct ast *check_list(void)
{
    struct ast *ast_list = ast_init();
    ast_list->type = NODE_LIST;
    ast_list->data.node_list = ast_init_list();
    size_t nbchild = ast_list->data.node_list->nbchild;
    while ((ast_list->data.node_list->childlist[nbchild] = check_andor())
           != NULL)
    {
        ast_list->data.node_list->nbchild++;
        nbchild++;
        if (s->lexer->current->type == SEMI_COLON
            && s->lexer->current->next->type == SEMI_COLON)
        {
        }
        else if (s->lexer->current->type == SEMI_COLON
                 || s->lexer->current->type == ESPERLUETTE)
        {
            get_next_token(s->lexer);
        }
        else
            break;
    }
    if (nbchild == 0)
    {
        ast_free(ast_list);
        return NULL;
    }
    return ast_list;
}

/**
** \fn static struct ast *check_andor_aux(struct ast *nouveau_noeud_andor)
** \brief fct auxiliaire pour le parsing and_or
**
** \param struct ast *nouveau_noeud_andor: ast à remplir
** \return ast bien rempli oui je pense
*/
static struct ast *check_andor_aux(struct ast *nouveau_noeud_andor)
{
    struct ast *jsp = ast_init();
    jsp->type = NODE_AND_OR;
    struct ast_and_or *andor = ast_init_and_or();
    jsp->data.node_and_or = andor;

    int a = (strcmp((s->lexer->current->word), "&&") == 0) ? 1 : 0;
    andor->and_or_or = a;
    get_next_token(s->lexer);
    while (s->lexer->current->type == SPACE_LINE)
        get_next_token(s->lexer);

    struct ast *rappel = check_pipeline();
    if (rappel == NULL)
    {
        ast_free(nouveau_noeud_andor);
        return NULL;
    }
    andor->left = nouveau_noeud_andor->data.node_and_or->left;
    andor->right = nouveau_noeud_andor->data.node_and_or->right;
    nouveau_noeud_andor->data.node_and_or->left = jsp;
    nouveau_noeud_andor->data.node_and_or->right = rappel;
    int tmp = nouveau_noeud_andor->data.node_and_or->and_or_or;
    nouveau_noeud_andor->data.node_and_or->and_or_or = andor->and_or_or;
    andor->and_or_or = tmp;

    return nouveau_noeud_andor;
}

/**
** \fn struct ast *check_andor(void)
** \brief parsing du and_or
**
** \param void.
** \return ast bien rempli oui je pense
*/
struct ast *check_andor(void)
{
    struct ast *premiere_pip = check_pipeline();
    if (!premiere_pip)
    {
        ast_free(premiere_pip);
        return NULL;
    }
    if (s->lexer->current->type != TOK_ANDOR)
        return premiere_pip; // on a eu 1 seule pipeline

    struct ast *nouveau_noeud_andor = ast_init();
    nouveau_noeud_andor->type = NODE_AND_OR;

    int b = (strcmp((s->lexer->current->word), "&&") == 0) ? 1 : 0;
    nouveau_noeud_andor->data.node_and_or = ast_init_and_or();
    nouveau_noeud_andor->data.node_and_or->and_or_or = b;
    get_next_token(s->lexer);
    while (s->lexer->current->type == SPACE_LINE)
        get_next_token(s->lexer);
    nouveau_noeud_andor->data.node_and_or->left = premiere_pip;
    nouveau_noeud_andor->data.node_and_or->right = check_pipeline();
    if (nouveau_noeud_andor->data.node_and_or->right == NULL)
    {
        ast_free(nouveau_noeud_andor);
        return NULL;
    }
    while (s->lexer->current->type == TOK_ANDOR)
    {
        nouveau_noeud_andor = check_andor_aux(nouveau_noeud_andor);
        if (!nouveau_noeud_andor)
            return NULL;
    }
    return nouveau_noeud_andor;
}

/**
** \fn struct ast *check_pipeline(void)
** \brief parsing des pipelines
**
** \param void.
** \return ast bien rempli oui je pense
*/
struct ast *check_pipeline(void)
{
    struct ast *ast_pipeline = ast_init();
    ast_pipeline->type = NODE_PIPELINE;
    ast_pipeline->data.node_pipeline = ast_init_pipeline();
    if (s->lexer->current->type == EXCLAMATION) // next
    {
        get_next_token(s->lexer);
        ast_pipeline->data.node_pipeline->neg = 1;
    }
    size_t nbchild = 0;
    if (!(ast_pipeline->data.node_pipeline->childlist[nbchild] =
              check_command()))
    {
        return surprise(ast_pipeline);
    }
    ast_pipeline->data.node_pipeline->nbchild++;
    nbchild = ast_pipeline->data.node_pipeline->nbchild;
    if (s->lexer->current->type != PIPE)
        return ast_pipeline;
    while (s->lexer->current->type == PIPE)
    {
        get_next_token(s->lexer);
        while (s->lexer->current->type == SPACE_LINE)
            get_next_token(s->lexer);
        if (!(ast_pipeline->data.node_pipeline->childlist[nbchild] =
                  check_command()))
        {
            return surprise(ast_pipeline);
        }
        ast_pipeline->data.node_pipeline->nbchild++;
        nbchild = ast_pipeline->data.node_pipeline->nbchild;
    }
    return ast_pipeline;
}

/**
** \fn struct ast *check_command(void)
** \brief parsing des commands
**
** \param void.
** \return ast bien rempli oui je pense
*/
struct ast *check_command(void)
{
    struct ast *ast_command = ast_init();
    ast_command->type = NODE_COMMAND;
    ast_command->data.node_command = ast_init_command();
    if ((ast_command->data.node_command->simple_or_shell_cmd =
             check_simple_command()))
    {
        return ast_command;
    }
    else if ((ast_command->data.node_command->simple_or_shell_cmd =
                  check_shell_command()))
    {
        struct ast_redir *test = ast_init_redir();
        test = check_redirection(test);
        if (test->is_finishe == 1)
        {
            ast_free_redir(test);
            return ast_command;
        }
        while (test->is_finishe == 0)
        {
            test = check_redirection(test);
        }
        struct ast *assst = ast_init();
        assst->type = NODE_REDIR;
        assst->data.node_redir = test;
        ast_command->data.node_command->redirection = assst;
        return ast_command;
    }
    ast_free(ast_command);
    return NULL;
}
