#define _POSIX_C_SOURCE 200809L
#include <string.h>

#include "parser.h"

/**
** \fn static struct ast
*vincent_auxiliaire_de_la_fonction_pour_simple_commande( struct ast
*ast_simple_command, int valid)
** \brief fct auxiliaire pour les WORD de simple_command
**
** \param ast *ast_simple_command: ast à remplir. int valid: 1 si c'est valid.
** \return ast rempli bien ou quoi
*/
static struct ast *vincent_auxiliaire_de_la_fonction_pour_simple_commande(
    struct ast *ast_simple_command, int valid)
{
    if (s->lexer->current->type == WORD)
    {
        valid = 1;
        char *cmd = strdup(s->lexer->current->word);
        get_next_token(s->lexer);
        while (s->lexer->current->type != SPACE_LINE
               && s->lexer->current->type != SEMI_COLON
               && s->lexer->current->type != TOKEN_EOF
               && s->lexer->current->type != ESPERLUETTE
               && s->lexer->current->type != TOK_ANDOR
               && s->lexer->current->type != PIPE
               && s->lexer->current->type != REDIR)
        {
            char *tmp = s->lexer->current->word;
            cmd = realloc(cmd, strlen(tmp) + strlen(cmd) + 2);
            cmd = strcat(cmd, " ");
            cmd = strcat(cmd, tmp);
            get_next_token(s->lexer);
        }
        ast_simple_command->data.node_simple_cmd->cmd = cmd;
    }
    if (valid == 0 || s->lexer->current->type == UNVALID)
    {
        ast_free(ast_simple_command);
        return NULL;
    }
    return ast_simple_command;
}

/**
** \fn struct ast *check_simple_command(void)
** \brief parsing du simple command
**
** \param void.
** \return ast rempli bien ou quoi
*/
struct ast *check_simple_command(void)
{
    struct ast *ast_simple_command = ast_init();
    ast_simple_command->type = NODE_SIMPLE_COMMAND;
    ast_simple_command->data.node_simple_cmd = ast_init_simple_command();
    struct token *current = s->lexer->current;
    int valid = 0;

    if (current == NULL)
    {
        ast_free(ast_simple_command);
        return NULL;
    }
    if (s->lexer->current->type == ASSIGN_WORD)
    {
        valid = 1;
        char *prefix = calloc(strlen(s->lexer->current->word) + 1, 1);
        prefix = strcat(prefix, s->lexer->current->word);
        add_prefix_ast_scmd(ast_simple_command->data.node_simple_cmd, prefix);
        while (get_next_token(s->lexer)->type == ASSIGN_WORD)
        {
            char *prefix2 = calloc(strlen(s->lexer->current->word) + 1, 1);
            prefix2 = strcat(prefix2, s->lexer->current->word);
            add_prefix_ast_scmd(ast_simple_command->data.node_simple_cmd,
                                prefix2);
        }
    }
    return vincent_auxiliaire_de_la_fonction_pour_simple_commande(
        ast_simple_command, valid);
}

/**
** \fn int check_valid_name(char *word)
** \brief regarde si un WORD est un nom bien pour une variable
**
** \param char* word: nom de la variable
** \return 0 si c'est un bon nom, 1 sinon
*/
int check_valid_name(char *word)
{
    if (*word >= '0' && *word <= '9')
        return 1;
    for (int i = 1; (word[i] != '\0') && (word[i] != '='); i++)
    {
        if (!((word[i] == '_') || (word[i] >= 'A' && word[i] <= 'z')
              || (word[i] >= '0' && word[i] <= '9')))
            return 1;
    }
    return 0;
}
