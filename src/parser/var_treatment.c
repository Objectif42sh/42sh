#include <stdio.h>

#include "../shell.h"
#include "parser.h"
#include "var.h"

/**
** \fn char *search_var(char *name)
** \brief return la valeur d'une variable à partir de son nom en paramètre
**
** \param char* name: nom de la variable
** \return la valeur de la variable
*/
char *search_var(char *name)
{
    struct var_item *tmp = s->vars->list;
    while (tmp && (strcmp(tmp->name, name) != 0))
        tmp = tmp->next;
    if (tmp == NULL)
    {
        char *res = calloc(1, 1);
        return res;
    }
    char *res = calloc(1, strlen(tmp->value) + 1);
    res = strcpy(res, tmp->value);
    return res;
}

/**
** \fn int var_index(const char *name)
** \brief return l'index du début de la varibale dans un mot
**
** \param char* word: commande à éxecuter
** \return la valeur de la variable
*/
int var_index(const char *word)
{
    size_t res = 0;
    if (word == NULL || *word == '\0' || word[1] == '\0')
        return -1;
    int not_found = 1;
    while (not_found)
    {
        while (word[res] != '$')
        {
            if (word[res] == '\\' && word[res + 1] == '\'')
            {
                res += 2;
                continue;
            }
            if (word[res] == '\'')
            {
                res++;
                while (word[res] != '\'')
                    res++;
            }
            res++;
            if (word[res] == '\0')
                return -1;
        }
        if (word[res + 1] != ' ' && word[res + 1] != '}')
            return res;
        else
            res++;
    }
    return res;
}

/**
** \fn int contain_var(const char *word)
** \brief dis oui si ya une variable dans un mot
**
** \param const char* word: commande
** \return 1 si ya une variable, 0 sinon.
*/
int contain_var(const char *word)
{
    int i = var_index(word);

    if (i == -1)
        return 0;

    if (word[i + 1] != '\0' && word[i + 1] != ' ' && word[i + 1] != '$'
        && word[i + 1] != '}')
        return 1;

    return 0;
}

static char *var_replace_aux1(size_t var_length, int i, const char *word,
                              char *bf)
{
    char *new_word;
    var_length++;
    const char *var_begin = word + i + 1;
    while (word[i] != '}')
        i++;

    var_length = i - var_length;
    char *var_name = calloc(var_length + 1, 1);
    var_name = strncpy(var_name, var_begin, var_length);
    char *var_value = search_var(var_name);
    size_t word_len = strlen(word);
    char *after = calloc(word_len - i, 1);
    after = strncpy(after, word + i + 1, word_len - i - 1);
    new_word = calloc(strlen(bf) + strlen(var_value) + strlen(after) + 1, 1);
    new_word = strcat(new_word, bf);
    new_word = strcat(new_word, var_value);
    new_word = strcat(new_word, after);

    free(bf);
    free(var_value);
    free(after);
    free(var_name);

    if (contain_var(new_word))
    {
        char *new_new_word = var_replace(new_word);
        free(new_word);
        return new_new_word;
    }
    return new_word;
}
static char *var_replace_aux2(size_t var_length, int i, const char *word,
                              char *bf)
{
    char *new_word;
    const char *var_begin = word + i;
    while (word[i] != ' ' && word[i] != '\0' && word[i] != '$' && word[i] != '}'
           && word[i] != '{')
        i++;

    var_length = i - var_length;
    char *var_name = calloc(var_length + 1, 1);
    var_name = strncpy(var_name, var_begin, var_length);
    char *var_value = search_var(var_name);
    size_t word_len = strlen(word);
    char *after = calloc(word_len - i + 1, 1);
    after = strncpy(after, word + i, word_len - i);

    new_word = calloc(strlen(bf) + strlen(var_value) + strlen(after) + 1, 1);

    new_word = strcat(new_word, bf);
    new_word = strcat(new_word, var_value);
    new_word = strcat(new_word, after);

    free(bf);
    free(var_value);
    free(after);
    free(var_name);

    if (contain_var(new_word))
    {
        char *new_new_word = var_replace(new_word);
        free(new_word);
        return new_new_word;
    }
    return new_word;
}

/**
** \fn char *var_replace(const char *word)
** \brief return la valeur d'une variable à partir de son nom en paramètre
**
** \param const char* word: commande
** \return la commande avec les valeurs des variables
*/
char *var_replace(const char *word)
{
    int i = var_index(word);
    if (i == -1)
        return NULL;
    char *before = calloc(sizeof(char), i + 1);
    before = strncpy(before, word, i);
    i++;
    if (word[i] == '\0')
        return NULL;
    size_t var_length = i;
    if (word[i] == '{')
    {
        return var_replace_aux1(var_length, i, word, before);
    }

    else if (word[i] != '\0' && word[i] != ' ')
    {
        return var_replace_aux2(var_length, i, word, before);
    }

    char *worddefault = malloc(1);
    return worddefault;
}
