#define _POSIX_C_SOURCE 200809L
#include <string.h>

#include "parser.h"

/**
** \fn char **add_values(char **values, char *str)
** \brief ajoute une variable dans la liste values
**
** \param char** values: liste de variable. char* str: variable à ajouter
** \return la liste
*/
char **add_values(char **values, char *str)
{
    size_t size = 0;
    if (str == NULL)
        return values;
    if (values)
    {
        for (size = 0; values[size]; ++size)
        {
            ;
        }
    }
    size++;
    values = realloc(values, (size + 1) * sizeof(char *));
    values[size - 1] = str;
    values[size] = NULL;
    return values;
}

/**
** \fn static struct ast *elif_aux(struct ast *ast_for)
** \brief fct auxiliaire pour for
**
** \param ast* ast_for: ast à remplir
** \return ast rempli
*/
static struct ast *elif_aux(struct ast *ast_for)
{
    get_next_token(s->lexer);
    while (s->lexer->current->type == SPACE_LINE)
        get_next_token(s->lexer);
    char **values = NULL;
    for (size_t i = 0; s->lexer->current->type == WORD; i++)
    {
        values = add_values(values, s->lexer->current->word);
        ast_for->data.node_for->nb_values += 1;
        get_next_token(s->lexer);
    }
    ast_for->data.node_for->values = values;
    if (s->lexer->current->type == SEMI_COLON
        || s->lexer->current->type == SPACE_LINE)
    {
        get_next_token(s->lexer);
    }
    else
    {
        ast_free(ast_for);
        return NULL;
    }
    return ast_for;
}
/**
** \fn static struct ast *while_du_aux(struct ast *ast_for)
** \brief fct auxiliaire pour for
**
** \param ast* ast_for: ast à remplir
** \return ast rempli
*/
static struct ast *while_du_aux(struct ast *ast_for)
{
    while (s->lexer->current->type == SPACE_LINE)
        get_next_token(s->lexer);
    if (s->lexer->current->type == RESERVED_WORD
        && strcmp(s->lexer->current->word, "do") == 0)
    {
        get_next_token(s->lexer);
        if ((ast_for->data.node_for->execution = check_compound()))
        {
            if (s->lexer->current->type == RESERVED_WORD
                && strcmp(s->lexer->current->word, "done") == 0)
            {
                get_next_token(s->lexer);
                return ast_for;
            }
            s->parse_error = 1;
            ast_free(ast_for);
            return NULL;
        }
    }
    s->parse_error = 1;
    ast_free(ast_for);
    return NULL;
}
/**
** \fn struct ast *parse_for(void)
** \brief parsing for ou quoi ? oui
**
** \param void.
** \return ast rempli bien
*/
struct ast *parse_for(void)
{
    struct ast *ast_for = ast_init();
    ast_for->type = NODE_FOR;
    ast_for->data.node_for = ast_init_for();
    if (s->lexer->current->type == RESERVED_WORD
        && strcmp(s->lexer->current->word, "for") == 0)
    {
        get_next_token(s->lexer);
        if (s->lexer->current->type == WORD)
        {
            ast_for->data.node_for->variable = s->lexer->current->word;
            get_next_token(s->lexer);
            if (s->lexer->current->type == SEMI_COLON)
            {
                get_next_token(s->lexer);
            }
            else if (s->lexer->current->type == SPACE_LINE
                     || (s->lexer->current->type == RESERVED_WORD
                         && strcmp(s->lexer->current->word, "in") == 0))
            {
                if ((ast_for = elif_aux(ast_for)) == NULL)
                    return NULL;
            }
            else
            {
                s->parse_error = 1;
                ast_free(ast_for);
                return NULL;
            }
            if ((ast_for = while_du_aux(ast_for)) != NULL)
                return ast_for;
            return NULL;
        }
    }
    ast_free(ast_for);
    return NULL;
}
