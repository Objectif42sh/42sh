#include "parser.h"

/**
** \fn static struct ast *auxili(struct ast *ast_if)
** \brief fct auxiliaire du parsing du if
**
** \param ast* ast_if: ast à remplir
** \return ast rempli bien ou quoi
*/
static struct ast *auxili(struct ast *ast_if)
{
    if ((s->lexer->current->type == RESERVED_WORD
         && strcmp(s->lexer->current->word, "else") == 0)
        || (s->lexer->current->type == RESERVED_WORD
            && strcmp(s->lexer->current->word, "elif") == 0))
    {
        int els_ou_eli = 0;
        if (strcmp(s->lexer->current->word, "elif") == 0)
            els_ou_eli = 2;
        else
            els_ou_eli = 1;
        if (!(ast_if->data.node_if->else_condition =
                  check_else_clause(els_ou_eli)))
        {
            ast_free(ast_if);
            return NULL;
        }
    }
    if (s->lexer->current->type == RESERVED_WORD
        && strcmp(s->lexer->current->word, "fi") == 0)
    {
        get_next_token(s->lexer);
        return ast_if;
    }
    else
    {
        s->parse_error = 1;
        ast_free(ast_if);
        return NULL;
    }
}

/**
** \fn struct ast *check_if(void)
** \brief parsing du if
**
** \param void.
** \return ast rempli bien ou quoi
*/
struct ast *check_if(void)
{
    struct ast *ast_if = ast_init();
    ast_if->type = NODE_IF;
    ast_if->data.node_if = ast_init_if();
    if (s->lexer->current->type == RESERVED_WORD
        && strcmp(s->lexer->current->word, "if") == 0)
    {
        get_next_token(s->lexer);
        if ((ast_if->data.node_if->if_condition = check_compound()) != NULL)
        {
            if (s->lexer->current->type == RESERVED_WORD
                && strcmp(s->lexer->current->word, "then") == 0)
            {
                get_next_token(s->lexer);
                if ((ast_if->data.node_if->then_condition = check_compound()))
                {
                    return auxili(ast_if);
                }
                else
                {
                    ast_free(ast_if);
                    return NULL;
                }
            }
            else
            {
                s->parse_error = 1;
                ast_free(ast_if);
                return NULL;
            }
        }
        else
        {
            ast_free(ast_if);
            return NULL;
        }
    }
    ast_free(ast_if);
    return NULL;
}
