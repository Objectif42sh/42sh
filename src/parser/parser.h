#ifndef PARSER_H
#define PARSER_H
#include <err.h>
#include <errno.h>
#include <stdio.h>

#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#define KEYWORD_NUM 16
#include "../shell.h"
#include "var.h"

struct ast_redir *check_redirection(struct ast_redir *ast_redir);
struct ast *check_shell_command_aux(struct ast *ast_shell_command, int *i);
struct ast *check_shell_command(void);
int is_keyword(char *word);
int check_valid_name(char *word);
struct ast *check_input(void);
struct ast *check_simple_command(void);
int execution(char *cmd);
char *get_word(char *line, int *offset);
struct ast *check_list(void);
struct ast *check_andor(void);
struct ast *check_pipeline(void);
struct ast *check_command(void);
int check_valid_name(char *word);
int parse(void);
struct ast *check_elif_clause_aux(struct ast *ast_else_clause);
struct ast *check_elif_clause(void);
struct ast *check_if(void);
struct ast *check_compound_aux(struct ast *ast_compound_list, size_t nbchild);
struct ast *check_compound(void);
struct ast *parse_for(void);
struct ast *parse_case(void);
struct ast *check_case_clause(char *variable);
struct ast *check_case_item(char *variable);
struct ast *check_else_clause(int els_ou_eli);
#endif
