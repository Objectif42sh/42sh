#include "parser.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "../builtins/builtins.h"

/**
** \fn int execution(char *cmd)
** \brief execute les commandes
**
** \param char* cmd: commande à éxecuter
** \return la valeur de retour de execvp
*/
int execution(char *cmd)
{
    char *cmd_vrai[150];
    int i = 0;
    char *tok = strtok(cmd, " ");

    while (tok)
    {
        cmd_vrai[i] = tok;
        i++;
        tok = strtok(NULL, " ");
    }
    cmd_vrai[i] = NULL;
    int tmpbu;
    if ((tmpbu = is_a_builtin(cmd_vrai)) != -1)
    {
        return tmpbu;
    }
    fflush(NULL);
    int pid = fork();
    errno = 0;
    if (pid == 0)
    {
        int tmp = execvp(*cmd_vrai, cmd_vrai);
        if (tmp == -1)
        {
            fprintf(stderr, "42sh: %s: command not found\n", *cmd_vrai);
            exit(127);
        }
        exit(0);
    }
    int wstatus;
    waitpid(pid, &wstatus, 0);
    s->return_value = WEXITSTATUS(wstatus);
    return (WEXITSTATUS(wstatus));
}

/**
** \fn int is_keyword(char *cmd)
** \brief check si un mot est réservé
**
** \param char* word: mot à analyser
** \return 0 si c'est pas un keyword, 1 si oui.
*/
int is_keyword(char *word)
{
    const char *keywords[KEYWORD_NUM + 1] = { "!",    "do",   "esac", "in",
                                              "{",    "done", "fi",   "then",
                                              "}",    "elif", "for",  "until",
                                              "case", "else", "if",   "while" };
    for (size_t i = 0; i < KEYWORD_NUM; i++)
    {
        if (strcmp(word, keywords[i]) == 0)
            return 1;
    }
    return 0;
}

/**
** \fn int word_or_assignment_word(char *word)
** \brief check si un mot est un prefix ou element ou quoi
**
** \param char* word: mot à analyser
** \return le type en question
*/
int word_or_assignment_word(char *word)
{
    if (*word == 0 || strlen(word) == 0 || *word == '\n' || *word == EOF)
        return UNVALID;
    if (strchr(word, '=') != NULL)
        return ASSIGN_WORD;
    if (check_valid_name(word) == 0)
        return WORD;
    return UNVALID;
}

/**
** \fn int parse(void)
** \brief fonction principale qui fait tout quoi
**
** \param void, rempli la variable globale.
** \return 0 si tout s'est bien passé, 1 sinon.
*/
int parse(void)
{
    if (s->parse_error == 1)
        return 1;

    if (s->lexer->token_list == NULL || s->lexer->token_list->type == TOKEN_EOF)
    {
        fprintf(stderr, "Le lexer est vide / fichier vide\n");
        return 1;
    }
    while (get_next_type(s->lexer) != TOKEN_EOF)
    {
        struct ast *input = check_input();
        if (s->parse_error == 1)
            return 1;
        if (input != NULL)
        {
            if (input->type != NODE_NULL)
            {
                s->astheads[s->nbchild++] = input;
                ast_eval(input);
                int zero = 0;
                s->breakflag = 0;
                s->continueflag = zero;
            }
        }
        else
            return 1;
    }
    return 0;
}
