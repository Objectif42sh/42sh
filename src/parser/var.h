#ifndef VAR_H
#define VAR_H
#define _POSIX_C_SOURCE 200809L
#include <string.h>

#include "../shell.h"
#include "parser.h"
struct var_item
{
    char *name;
    char *value;
    struct var_item *next;
};

struct var_list
{
    struct var_item *list;
};

struct var_list *init_var_list(void);
struct var_item *assign_word_to_var(char *str);
void add_var(struct var_item *var);
void print_vars(struct var_list *var_list);
void free_vars();
char *search_var(char *name);
char *var_replace(const char *word);
struct ast *parse_while();
struct ast *parse_until();
int contain_var(const char *word);
#endif
