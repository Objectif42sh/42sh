#include "parser.h"

/**
** \fn struct ast *check_elif_clause_aux(struct ast *ast_else_clause)
** \brief fct auxiliaire pour le parsing du elif
**
** \param ast *ast_else_clause: ast à remplir
** \return ast rempli
*/
struct ast *check_elif_clause_aux(struct ast *ast_else_clause)
{
    if ((s->lexer->current->type == RESERVED_WORD
         && strcmp(s->lexer->current->word, "else") == 0)
        || (s->lexer->current->type == RESERVED_WORD
            && strcmp(s->lexer->current->word, "elif") == 0))
    {
        int els_eli = 0;
        if (strcmp(s->lexer->current->word, "elif") == 0)
            els_eli = 2;
        else
            els_eli = 1;
        if (!(ast_else_clause->data.node_else_clause->else_clause =
                  check_else_clause(els_eli)))
        {
            ast_free(ast_else_clause);
            return NULL;
        }
    }
    if (s->lexer->current->type == RESERVED_WORD
        && strcmp(s->lexer->current->word, "fi") == 0)
        return ast_else_clause;
    s->parse_error = 1;
    ast_free(ast_else_clause);
    return NULL;
}

/**
** \fn ast *check_compound_aux(struct ast *ast_compound_list, size_t nbchild)
** \brief fct auxiliaire pour le parsing du compound_list
**
** \param ast *ast_compound_list: ast à remplir. nbchild: la taille de la liste
** \return ast rempli bien ou quoi
*/
struct ast *check_compound_aux(struct ast *ast_compound_list, size_t nbchild)
{
    while ((ast_compound_list->data.node_compound_list->childlist[nbchild] =
                check_andor()))
    {
        nbchild++;
        ast_compound_list->data.node_compound_list->nbchild++;
        if (s->lexer->current->type == SEMI_COLON
            && s->lexer->current->next->type == SEMI_COLON)
        {
            break;
        }
        else if (s->lexer->current->type == SEMI_COLON
                 || s->lexer->current->type == ESPERLUETTE
                 || s->lexer->current->type == SPACE_LINE)
        {
            get_next_token(s->lexer);
            while (s->lexer->current->type == SPACE_LINE)
                get_next_token(s->lexer);
        }
        else
        {
            break;
        }
    }
    if (nbchild == 0)
    {
        ast_free(ast_compound_list);
        return NULL;
    }
    return ast_compound_list;
}

/**
** \fn struct ast *check_shell_command_aux(struct ast *ast_shell_command, int
    i)
** \brief fct auxiliaire pour le parsing des shell commands
**
** \param ast *ast_shell_command: ast à remplir. int* i: je sais pas
** \return ast rempli bien ou quoi
*/
struct ast *check_shell_command_aux(struct ast *ast_shell_command, int *i)
{
    struct token *current = s->lexer->current;

    if (strcmp(current->word, "{") == 0)
    {
        get_next_token(s->lexer);
        if ((ast_shell_command->data.node_shell_command->compound_or_rule =
                 check_compound()))
        {
            current = get_next_token(s->lexer);
            if (strcmp(current->word, "}") == 0)
            {
                *i = 1;
                return ast_shell_command;
            }
        }
        s->parse_error = 1;
        ast_free(ast_shell_command);
        return NULL;
    }
    if (strcmp(current->word, "(") == 0)
    {
        get_next_token(s->lexer);
        if ((ast_shell_command->data.node_shell_command->compound_or_rule =
                 check_compound()))
        {
            current = get_next_token(s->lexer);
            if (strcmp(current->word, ")") == 0)
            {
                *i = 1;
                return ast_shell_command;
            }
            s->parse_error = 1;
            ast_free(ast_shell_command);
            return NULL;
        }
    }
    return ast_shell_command;
}
