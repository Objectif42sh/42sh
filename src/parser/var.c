#include "var.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parser.h"

/**
** \fn struct var_list *init_var_list(void)
** \brief initialise la liste des variables
**
** \param void.
** \return la commande avec les valeurs des variables
*/
struct var_list *init_var_list(void)
{
    struct var_list *vars = malloc(sizeof(struct var_list));
    if (vars == NULL)
        return NULL;
    vars->list = NULL;
    return vars;
}

/**
** \fn struct var_item *assign_word_to_var(char *str)
** \brief transforme un char* en struct var_item
**
** \param char* str: var=8 par exemple oui
** \return une structure var_item bien pleine
*/
struct var_item *assign_word_to_var(char *str)
{
    struct var_item *new_var = malloc(sizeof(struct var_item));
    new_var->name = strdup(strtok(str, "="));
    if (str[strlen(new_var->name) + 1] == '\0')
        new_var->value = calloc(1, 1);
    else
        new_var->value = strdup(strtok(NULL, "="));
    new_var->next = NULL;
    free(str);
    return new_var;
}

/**
** \fn void add_var(struct var_item *var)
** \brief ajoute une variable à la liste globale
**
** \param var_item *var: la variable à ajouter
** \return void.
*/
void add_var(struct var_item *var)
{
    struct var_list *tmp = s->vars;
    struct var_item *item = tmp->list;
    if (item == NULL)
    {
        tmp->list = var;
        return;
    }
    while (item->next != NULL)
    {
        if (strcmp(item->name, var->name) == 0)
        {
            free(item->value);
            item->value = strdup(var->value);
            free(var->value);
            free(var->name);
            free(var);
            return;
        }
        item = item->next;
    }
    if (strcmp(item->name, var->name) == 0)
    {
        free(item->value);
        item->value = strdup(var->value);
        free(var->value);
        free(var->name);
        free(var);
        return;
    }
    item->next = var;
}

/**
** \fn void free_vars(void)
** \brief free toutes les variables
**
** \param void.
** \return void.
*/
void free_vars(void)
{
    if (s->vars == NULL)
        return;
    struct var_list *tmp = s->vars;
    struct var_item *cur = tmp->list;
    if (cur == NULL)
        return;
    if (cur->next == NULL)
    {
        free(cur->name);
        free(cur->value);
        free(cur);
        return;
    }
    struct var_item *prev = tmp->list;
    cur = cur->next;
    while (cur->next != NULL)
    {
        free(prev->value);
        free(prev->name);
        free(prev);
        prev = cur;
        cur = cur->next;
    }
    free(prev->name);
    free(prev->value);
    free(prev);
    free(cur->value);
    free(cur->name);
    free(cur);
}

/**
** \fn void print_vars(struct var_list *var_list)
** \brief print les variables
**
** \param var_list *var_list: liste des varibales à print
** \return void.
*/
void print_vars(struct var_list *var_list)
{
    struct var_item *hhead = var_list->list;
    for (int i = 1; hhead != NULL; i++, hhead = hhead->next)
    {
        puts("--------");
        printf("index : %d\n", i);
        printf("name : %s\n", hhead->name);
        printf("value : %s\n", hhead->value);
        puts("--------");
        if (hhead->next)
        {
            puts("   ||   ");
            puts("   ||next");
            puts("   ||   ");
            puts("   \\/   ");
        }
    }
}
