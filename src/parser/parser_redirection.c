#include "parser.h"

/**
** \fn int get_ionumber(char *word)
** \brief trouve l'io number dans une redirection
**
** \param char* word: la redirection
** \return 1 par défaut.
*/
int get_ionumber(char *word)
{
    for (int i = 0; word[i] != '\0'; i++)
    {
        if (word[i] == '>')
        {
            if (i == 0)
                return 1;
            return (word[i - 1] - '0');
        }
    }
    return -1;
}

/**
** \fn struct ast_redir *check_redirection(struct ast_redir *ast_redir)
** \brief parse les redirections.
**
** \param struct ast_redir *ast_redir: ast à remplir
** \return ast rempli ou quoi
*/
struct ast_redir *check_redirection(struct ast_redir *ast_redir)
{
    if (s->lexer->current->type == REDIR)
    {
        int ionumber = get_ionumber(s->lexer->current->word);
        get_next_token(s->lexer);
        if (s->lexer->current->type == WORD)
        {
            if (ionumber == 0)
                ast_redir->redirIO_0 = s->lexer->current->word;
            else if (ionumber == 2)
                ast_redir->redirIO_2 = s->lexer->current->word;
            else
                ast_redir->redirIO_1 = s->lexer->current->word;
            get_next_token(s->lexer);
            return ast_redir;
        }
        else
            s->parse_error = 1;
    }
    ast_redir->is_finishe = 1;
    return ast_redir;
}
