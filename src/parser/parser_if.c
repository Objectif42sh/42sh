#include "parser.h"

/**
** \fn static void *surprise(struct ast *ast)
** \brief fct auxiliaire free et return NULL dans les cas d'erreurs
**
** \param ast *ast: ast à free
** \return NULL
*/
static void *surprise(struct ast *ast)
{
    ast_free(ast);
    return NULL;
}

/**
** \fn struct ast *check_elif_clause(void)
** \brief fct qui parse les elif
**
** \param void: utlise la var globales s
** \return ast rempli bien ou quoi
*/
struct ast *check_elif_clause(void)
{
    struct ast *ast_else_clause = ast_init();
    ast_else_clause->type = NODE_ELSE_CLAUSE;
    ast_else_clause->data.node_else_clause = ast_init_else_clause();
    ast_else_clause->data.node_else_clause->is_else = 2;
    if (s->lexer->current->type == RESERVED_WORD
        && strcmp(s->lexer->current->word, "elif") == 0)
    {
        get_next_token(s->lexer);
        if ((ast_else_clause->data.node_else_clause->elif_condition =
                 check_compound())
            != NULL)
        {
            if (s->lexer->current->type == RESERVED_WORD
                && strcmp(s->lexer->current->word, "then") == 0)
            {
                get_next_token(s->lexer);
                if ((ast_else_clause->data.node_else_clause->then_condition =
                         check_compound()))
                    return check_elif_clause_aux(ast_else_clause);
                ast_free(ast_else_clause);
                return NULL;
            }
            s->parse_error = 1;
            ast_free(ast_else_clause);
            return NULL;
        }
        ast_free(ast_else_clause);
        return NULL;
    }
    ast_free(ast_else_clause);
    return NULL;
}

/**
** \fn struct ast *check_else_clause_aux(void)
** \brief fct auxiliaire qui parse les else
**
** \param void: utlise la var globales s
** \return ast rempli bien ou quoi
*/
struct ast *check_else_clause_aux(void)
{
    struct ast *ast_else_clause = ast_init();
    ast_else_clause->type = NODE_ELSE_CLAUSE;
    ast_else_clause->data.node_else_clause = ast_init_else_clause();
    ast_else_clause->data.node_else_clause->is_else = 1;
    struct token *current = s->lexer->current;
    if (current->type == RESERVED_WORD && strcmp(current->word, "else") == 0)
    {
        get_next_token(s->lexer);
        if ((ast_else_clause->data.node_else_clause->else_comp_list =
                 check_compound())
            != NULL)
        {
            return ast_else_clause;
        }
        else
        {
            ast_free(ast_else_clause);
            return NULL;
        }
    }
    ast_free(ast_else_clause);
    return NULL;
}

/**
** \fn struct ast *check_else_clause(int els_ou_eli)
** \brief fct qui parse les else
**
** \param int els_ou_eli: 0 si else, 1 si elif
** \return ast rempli bien ou quoi
*/
struct ast *check_else_clause(int els_ou_eli)
{
    if (els_ou_eli == 2)
    {
        return check_elif_clause();
    }
    else if (els_ou_eli == 1)
    {
        return check_else_clause_aux();
    }
    return NULL;
}

/**
** \fn struct ast *check_compound(void)
** \brief fct qui parse les compound_list
**
** \param void.
** \return ast rempli bien ou quoi
*/
struct ast *check_compound(void)
{
    struct ast *ast_compound_list = ast_init();
    ast_compound_list->type = NODE_COMPOUND_LIST;
    ast_compound_list->data.node_compound_list = ast_init_compound_list();
    while (s->lexer->current->type == SPACE_LINE)
        get_next_token(s->lexer);
    size_t nbchild = ast_compound_list->data.node_compound_list->nbchild;
    ast_compound_list = check_compound_aux(ast_compound_list, nbchild);
    if (!ast_compound_list)
        return NULL;
    if (s->lexer->current->type == SEMI_COLON
        && s->lexer->current->next->type == SEMI_COLON)
        return ast_compound_list;
    else if (s->lexer->current->type == SEMI_COLON
             || s->lexer->current->type == ESPERLUETTE
             || s->lexer->current->type == SPACE_LINE)
    {
        get_next_token(s->lexer);
        while (s->lexer->current->type == SPACE_LINE)
            get_next_token(s->lexer);
    }
    return ast_compound_list;
}

/**
** \fn struct ast *check_shell_command(void)
** \brief fct qui parse les shell commands
**
** \param void.
** \return ast rempli bien ou quoi
*/
struct ast *check_shell_command(void)
{
    struct ast *ast_shell_command = ast_init();
    ast_shell_command->type = NODE_SHELL_COMMAND;
    ast_shell_command->data.node_shell_command = ast_init_shell_command();
    int teste = 0;
    ast_shell_command = check_shell_command_aux(ast_shell_command, &teste);

    if (ast_shell_command == NULL)
        return NULL;
    else if (teste == 1)
        return ast_shell_command;
    if ((ast_shell_command->data.node_shell_command->compound_or_rule =
             check_if()))
        return ast_shell_command;
    else if ((ast_shell_command->data.node_shell_command->compound_or_rule =
                  parse_while()))
        return ast_shell_command;
    else if ((ast_shell_command->data.node_shell_command->compound_or_rule =
                  parse_until()))
        return ast_shell_command;
    else if ((ast_shell_command->data.node_shell_command->compound_or_rule =
                  parse_for()))
        return ast_shell_command;
    else if ((ast_shell_command->data.node_shell_command->compound_or_rule =
                  parse_case()))
        return ast_shell_command;
    return surprise(ast_shell_command);
}
