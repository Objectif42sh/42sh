#include "shell.h"

#include "parser/var.h"

/**
** \fn int struct shell *init_shell(void)
** \brief fonction initialisation de notre variable globale shell
**
** \param void pas de parametre
** \return une nouvelle struct shell initialisee
*/
struct shell *init_shell(void)
{
    struct shell *s = malloc(sizeof(struct shell));
    s->vars = init_var_list();
    s->lexer = init_lexer();
    s->nbchild = 0;
    s->return_value = 0;
    s->parse_error = 0;
    s->redirecsion = NULL;
    s->redirecsion_cmd = NULL;
    s->fdflag = NULL;
    s->breakflag = 0;
    s->continueflag = 0;
    s->line = NULL;
    s->file = NULL;

    return s;
}

/**
** \fn void ast_astheads_free(void)
** \brief fonction qui free tous les ast contenus dans la struct shell
**
** \param void pas de parametre
** \return void car cest un free
*/
void ast_astheads_free(void)
{
    for (size_t i = 0; i < s->nbchild; i++)
        ast_free(s->astheads[i]);
}

/**
** \fn void free_shell(void)
** \brief fonction qui free toute notre struct shell
**
** \param void pas de parametre
** \return void car cest un free
*/
void free_shell(void)
{
    if (s->fdflag)
        fclose(s->fdflag);
    if (s->line)
        free(s->line);
    if (s->file)
        free(s->file);
    free_lexer(s->lexer);
    free_vars(s);
    free(s->vars);
    ast_astheads_free();
    free(s);
}
